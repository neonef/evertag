using System;
using Xamarin.Forms;
using Android.Content;

namespace evertag
{
    public class MainPage : MasterDetailPage
	{
		public MasterPage masterPage {
			get;
			set;
		}

		public MainPage ()
		{
			masterPage = new MasterPage ();
			masterPage.ListView.ItemSelected += OnItemSelected;

			if (Device.OS == TargetPlatform.Windows) {
				Master.Icon = "swap.png";
			}

			Master = masterPage;

			BackgroundColor = Color.White;

			IsGestureEnabled = false;

			LaunchFolderPageAsNavigation();
		}

		void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
			if (item != null) {
				if (item.Title == "Browse by Folder") {
					Detail = new NavigationPage (
						new FolderPage (
							FolderPageBrowseMode.ByFolder
						)
					);//Search by Tag
				} else if (item.Title == "Search by Tag") {
					Detail = new NavigationPage (
						new SearchByTagPage ()
					);
				} else if (item.Title == "Files") {
					LaunchFolderPageAsNavigation ();
				} else if (item.Title == "Sync") {
					try {

                        Forms.Context.StartService (new Intent (Forms.Context, typeof(DropboxRefreshService)));

						LaunchFolderPageAsNavigation ();
					} catch (Exception) {
						
					}
				} else if (item.Title == "Reset Database") {
					DatabaseManager.RecreateDB ();
				} else if (item.Title == "Clouds") {
					Detail = new NavigationPage (
						new DatastorePage()
					) {
						BarBackgroundColor = Color.Blue,
					};

				}
				//Detail = new NavigationPage ((Page)Activator.CreateInstance (item.TargetType));
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}

		void LaunchFolderPageAsNavigation () {
			Detail = new NavigationPage (
				new FolderPage(
					FolderPageBrowseMode.ByTag
				)
			) {
				BarBackgroundColor = Color.Blue,
			};



//			var myNav = new NavigationPage (
//				new FolderPage(
//					FolderPageBrowseMode.ByTag
//				)
//			);
//			myNav.Popped += (object sender, NavigationEventArgs e) => {
//				if (TagList != null && TagList.Count > 0) {
//					TagList.RemoveAt(TagList.Count -1);
//				}
//			};
		}
	}

}

