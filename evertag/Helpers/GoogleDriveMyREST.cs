﻿//https://developers.google.com/identity/protocols/OAuth2InstalledApp#overview

using System;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;

namespace evertag
{
    public class GoogleDriveMyREST
	{
		private const string client_id = "30460193353-m5cm5p30j8of0d0pfnhq7eb164rhl4vr.apps.googleusercontent.com";
		private const string client_secrect = "UYTd8QDsGA9NI4JT0ah_7Dcf";
		private const string redirect_uri = "urn:ietf:wg:oauth:2.0:oob:auto";
		private const string auth_uri = "https://accounts.google.com/o/oauth2/auth";
		private const string token_uri = "https://accounts.google.com/o/oauth2/token";
		private const string about_uri = "https://www.googleapis.com/drive/v2/about";
		private const string files_uri = "https://www.googleapis.com/drive/v2/files";

		public static string GetLoginUrl() {
			//https://accounts.google.com/o/oauth2/v2/auth

			var scope = "https://www.googleapis.com/auth/drive"; //"email%20profile"; //https://www.googleapis.com/auth/plus.profile.emails.read
			var response_type = "code";
			var state = Guid.NewGuid().ToString("N");
			//var redirect_uri = "urn:ietf:wg:oauth:2.0:oob";


			var sb = new StringBuilder ();
//			sb.AppendFormat(
//				"https://accounts.google.com/o/oauth2/v2/auth?scope={0}&redirect_uri={1}&response_type={2}&client_id={3}",
//				scope, redirect_uri, response_type,client_id
//			);
			sb.AppendFormat(auth_uri + "?scope={0}&redirect_uri={1}&response_type={2}&client_id={3}",
				scope, redirect_uri, response_type,client_id
			);

			return sb.ToString ();
		}

		public static string GetLogoutUrl() {
			return "https://accounts.google.com/Logout";
		}

		public static async Task<GoogleRESTTokenResponse> GetToken(string code) {
			try {
				string content = "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,code,client_id,client_secrect,redirect_uri);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(token_uri);
				StringContent str = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded");
				var response = await client.PostAsync(token_uri, str);
				string stringResponse = await response.Content.ReadAsStringAsync();
				var tokenResponse = JsonConvert.DeserializeObject<GoogleRESTTokenResponse>(stringResponse);

				return tokenResponse;
			} catch (Exception ex) {
				var m = ex.Message;
				return null;
			}

		}

		public static async Task<GoogleRESTTokenResponse> RefreshToken(string refresh_token) {
			try {
				string content = "refresh_token={0}&client_id={1}&client_secret={2}&grant_type=refresh_token";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,refresh_token,client_id,client_secrect);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(token_uri);
				StringContent str = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded");
				var response = await client.PostAsync(token_uri, str);
				string stringResponse = await response.Content.ReadAsStringAsync();
				var tokenResponse = JsonConvert.DeserializeObject<GoogleRESTTokenResponse>(stringResponse);

				return tokenResponse;
			} catch (Exception ex) {
				var m = ex.Message;
				return null;
			}

		}

		public static async Task<GoogleDriveAccountInfo> GetAccountInfo(string token) {
			try {
				string content = "?access_token={0}&key={1}";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,token,client_id);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(about_uri);
				var response = await client.GetAsync(about_uri + content);
				string stringResponse = await response.Content.ReadAsStringAsync();
				var jsonResponse = JsonConvert.DeserializeObject<GoogleDriveAccountInfo>(stringResponse);

				return jsonResponse;
			} catch (Exception ex) {
				var m = ex.Message;
				return null;
			}

		}

		public static async Task<byte[]> GetFile(Datastore ds, string fileId) {
			try {
				string content = "/{2}?access_token={0}&key={1}&alt=media";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,ds.AccessToken,client_id,fileId);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(files_uri);
				var response = await client.GetAsync(files_uri + content);
				var bytes = await response.Content.ReadAsByteArrayAsync();
				return bytes;

			} catch (Exception ex) {
				var m = ex.Message;
				return null;
			}

		}

		public static async Task<bool> DeleteFile(Datastore ds, string fileId) {
			try {
				string content = "/{2}?access_token={0}&key={1}";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,ds.AccessToken,client_id,fileId);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(files_uri);
				var response = await client.DeleteAsync(files_uri + content);
				string stringResponse = await response.Content.ReadAsStringAsync();

				return string.IsNullOrEmpty(stringResponse);

			} catch (Exception ex) {
				var m = ex.Message;
				return false;
			}

		}

		public static async Task<List<GoogleDriveFile>> GetFiles(Datastore ds, string path = "") {
			try {
				string content = "?access_token={0}&key={1}&maxResults={2}";
				var sb = new System.Text.StringBuilder();
				sb.AppendFormat(content,ds.AccessToken,client_id,1000);
				content = sb.ToString();

				var client = new System.Net.Http.HttpClient();
				client.BaseAddress = new Uri(files_uri);
				var response = await client.GetAsync(files_uri + content);
				string stringResponse = await response.Content.ReadAsStringAsync();
				var jsonReponse = JsonConvert.DeserializeObject<GoogleDriveFileResponse>(stringResponse);

				return jsonReponse.items;

			} catch (Exception ex) {
				var m = ex.Message;
				return null;
			}

		}





	}

	public class GoogleRESTTokenRequest {
		public string code { get; set; }
		public string client_id { get; set; }
		public string client_secret { get; set; }
		public string redirect_uri { get; set; }
		public string grant_type { get; set; }
	}

	public class GoogleRESTTokenResponse {
		public string access_token { get; set; }
		public string refresh_token { get; set; }
		public int expires_in { get; set; }
		public string token_type { get; set; }
	}

	public class GoogleDriveAccountInfo {
		public string Name { get; set; }
		public GoogleDriveUser user { get; set; }
		public long quotaBytesTotal { get; set; }
		public long quotaBytesUsed { get; set; }
        public long quotaBytesUsedAggregate { get; set; }
    }

	public class GoogleDriveUser {
		public string displayName { get; set; }
		public string permissionId { get; set; }
		public string emailAddress { get; set; }
	}

	public class GoogleDriveFileResponse {
		public List<GoogleDriveFile> items { get; set; }
	}

	public class GoogleDriveFile {
		public string id { get; set; }
		public string title { get; set; }
		public string mimeType { get; set; }
		public long fileSize { get; set; }
		public string md5Checksum { get; set; }
		public string thumbnailLink { get; set; }
		public string downloadUrl { get; set; }

	}

}


// Json:
//{"installed":{"client_id":"30460193353-m5cm5p30j8of0d0pfnhq7eb164rhl4vr.apps.googleusercontent.com","project_id":"golden-walker-125216","auth_uri":"https://accounts.google.com/o/oauth2/auth","token_uri":"https://accounts.google.com/o/oauth2/token","auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs","client_secret":"UYTd8QDsGA9NI4JT0ah_7Dcf","redirect_uris":["urn:ietf:wg:oauth:2.0:oob","http://localhost"]}}

