﻿using System;
using Xamarin.Forms;
using FFImageLoading.Forms;
using System.Threading.Tasks;

namespace evertag
{
    public class ArchivoPage2 : ContentPage
	{
		Label tagText;
		public FolderPageItem fpi;
		Datastore ds;
		CachedImage  thumbnail;
		CachedImage downloadButton;
		CachedImage deleteButton;
		CachedImage tagsButton;

		public Carousel4 carousel { get; set; }

		public ArchivoPage2 (FolderPageItem myfpi, Datastore myds)
		{
			this.fpi = myfpi;
			this.ds = myds;
			Title = myfpi.Name;

			BackgroundColor = Color.White;

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

            //string source = "";
            //if (!String.IsNullOrEmpty(fpi.archivo.ThumbnailPath))
            //{
            //    source = fpi.archivo.ThumbnailPath;
            //}
            //else if (!String.IsNullOrEmpty(fpi.archivo.LocalPath))
            //{
            //    source = fpi.archivo.LocalPath;
            //}

            string source = ArchivoManager.GetThumbnailPath(fpi.archivo).Result;

			var h = App.ScreenHeight / 3;
			thumbnail = new CachedImage {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 300,
				HeightRequest = 300,
				CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				//Source = fpi.Path,
				//Source = fpi.archivo.ThumbnailPath,
                Source = source,
				LoadingPlaceholder = "wait_128.png",
			};
			thumbnail.Error += async (object sender, CachedImageEvents.ErrorEventArgs e) => {
				try {
                    //var db = DatabaseManager.GetNewAsyncConnection();
                    //thumbnail.Source = await ArchivoManager.DownloadThumbnail(fpi.archivo, db);

                    thumbnail.Source = await ArchivoManager.GetThumbnailPath(fpi.archivo, true);

				} catch (Exception ex) {
					var ee = ex.Message;
				}
			};

			tagText = new Label {
				HorizontalOptions = LayoutOptions.CenterAndExpand,
				TextColor = Color.Black,
				IsVisible = true,
				Text = fpi.archivo.TagDescription,
			};

			StackLayout nbar = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					tagText,
				}
			};


			tagsButton = new CachedImage {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 50,
				HeightRequest = 50,
				CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				Source = "tag_empty_128.png",
			};
			var tapGestureRecognizer = new TapGestureRecognizer ();
			tapGestureRecognizer.Tapped += async (sender, e) => {
				tagsButton.Source = "tag_full_128.png";
				var newpage = new ArchivoTagPage(fpi.Id);
				newpage.ArchivoPage2 = this;
				await Navigation.PushAsync (newpage);
				tagsButton.Source = "tag_empty_128.png";
			};
			tagsButton.GestureRecognizers.Add(tapGestureRecognizer);

			downloadButton = new CachedImage {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 50,
				HeightRequest = 50,
				CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				Source = "cloud_download_empty_128.png",
			};
			var tapGestureRecognizer2 = new TapGestureRecognizer ();
			tapGestureRecognizer2.Tapped += (sender, e) => {
				downloadButton.Source = "cloud_download_fill_128.png";
				//Toast.MakeText (this, "The demo service has started", ToastLength.Long).Show();
				Android.Widget.Toast.MakeText(Forms.Context, "Opening " + fpi.Name + " ...",  Android.Widget.ToastLength.Long).Show();
				ArchivoManager.Open(fpi.archivo, true);
				downloadButton.Source = "cloud_download_empty_128.png";
			};
			downloadButton.GestureRecognizers.Add(tapGestureRecognizer2);

			deleteButton = new CachedImage {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				WidthRequest = 50,
				HeightRequest = 50,
				CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				Source = "trash_empty_128.png",
			};
			var tapGestureRecognizer3 = new TapGestureRecognizer ();
			tapGestureRecognizer3.Tapped += async (sender, e) => {
				deleteButton.Source = "trash_fill_128.png";
				var res = await DisplayAlert(
					"Deleting a file",
					"Are you sure you want to delete " + fpi.Name,
					"Yes",
					"No"
				);
				if (res == true) {
					Android.Widget.Toast.MakeText(Forms.Context, "Deleting " + fpi.Name + " ...",  Android.Widget.ToastLength.Long).Show();
					var deleted = await ArchivoManager.Delete(ds, fpi.archivo, DatabaseManager.GetNewAsyncConnection());
					if (deleted && carousel != null) {
						carousel.DropPage(this);
					}
				}

				deleteButton.Source = "trash_empty_128.png";
			};
			deleteButton.GestureRecognizers.Add(tapGestureRecognizer3);

			// Accomodate iPhone status bar.
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			// Messaging
			MessagingCenter.Subscribe<TagCell, int> (this, "DeleteTag", async (sender, arg) => {
				await RefreshTagList();
			});
			MessagingCenter.Subscribe<ArchivoTagPage, int> (this, "ArchivoTagRelChanged", async (sender, arg) => {
				if (fpi.archivo.Id == arg) {
					await RefreshTagList();
				}
			});

			// Body
			StackLayout buttonsBar = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				HorizontalOptions = LayoutOptions.Center,
				Children = {
					tagsButton,
					downloadButton,
					deleteButton,
				}
			};

            
            Content = new StackLayout {
                Children = {
                    thumbnail,
                    nbar,
                    buttonsBar,
                    new Label {
                        Text = "Datastore: " + myds.Name
                    },
                    new Label {
                        Text = "Size: " + ArchivoManager.HumanSize(fpi.archivo.Size)
                    },
                }
			};

		}

		public async Task RefreshTagList() {
			var archivoTagList = await ArchivoManager.GetArchivoTags (fpi.archivo.Id, DatabaseManager.GetNewAsyncConnection());
			tagText.Text = "[" + TagManager.TagListAsStringNames (archivoTagList) + "]";
		}

	}
}

