﻿namespace evertag
{
    public class ArchivoTagPageItem
	{
		public int Id {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string Source {
			get;
			set;
		}

		public bool Assigned {
			get;
			set;
		}

		public ArchivoTagPageItemType Type {
			get;
			set;
		}

		public int[] archivoid_tagid {
			get;
			set;
		}
	}

	public enum ArchivoTagPageItemType {
		tag, create_tag
	}
}

