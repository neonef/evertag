﻿using System;
using SQLite;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text;

namespace evertag
{
    public class DatabaseManager
	{
		static object locker = new object ();
		//public SQLiteConnection db;


//		public DatabaseManager (bool recreate = false)
//		{
//			if (recreate)
//				File.Delete (dbpath);
//
//			// create the tables
//			bool exists = File.Exists (dbpath);
//
//			if (!exists) {
//				db = new SQLiteConnection (dbpath);
//
//				// Datastores
//				db.CreateTable<Datastore> ();
//				var nd = new Datastore {
//					Name = "Dropbox Neftalí", 
//					Type = DatastoreType.Dropbox,
//					AccessToken = "EfusKcuglx0AAAAAAAAPp-3UVpxG70XvsSabCeOIdr5_6rYdWUBMm1zuMX26b2zD"
//				};
//				var insert = db.Insert (nd);
//
//				// Tables
//				db.CreateTable<Archivo> ();
//				db.CreateTable<Archivo_Tag> ();
//				db.CreateTable<Tag> ();
//
//			}
//
//		}

		public static void CheckDB() {
			var exists = true;
			using (var db = GetNewConnection ()) {
				var tb1 = db.GetTableInfo("Archivo");
				if (tb1.Count == 0)
					exists = false;
			}
			if (!exists)
				RecreateDB ();
		}

		public static void RecreateDB() {

			try {
				using (var db = GetNewConnection ()) {
					
					//var r = db.Execute ("select 'drop table ' || name || ';' from sqlite_master where type = 'table';");
					int r;
					r = db.DropTable<Archivo_Tag>();
					r = db.DropTable<Tag>();
					r = db.DropTable<Archivo>();
					r = db.DropTable<Datastore>();
				
					// Datastores
					db.CreateTable<Datastore> ();
                    //var nd = new Datastore {
                    //	Name = "Dropbox Neftalí", 
                    //	Type = DatastoreType.Dropbox,
                    //	AccessToken = "EfusKcuglx0AAAAAAAAPp-3UVpxG70XvsSabCeOIdr5_6rYdWUBMm1zuMX26b2zD"
                    //};
     //               var nd = new Datastore
     //               {
     //                   Name = "Local Storage",
     //                   Type = DatastoreType.LocalStorage,
     //               };
					//var insert = db.Insert (nd);
				
					// Tables
					db.CreateTable<Archivo> ();
					db.CreateTable<Tag> ();
					db.CreateTable<Archivo_Tag> ();
				}
			} catch (SQLiteException ex) {
				var m = ex.Message;
			} catch (Exception ex) {
				var m = ex.Message;
			}
		}

		public static SQLiteConnection GetNewConnection() {
			return new SQLiteConnection (dbpath);
		}

		public static SQLiteAsyncConnection GetNewAsyncConnection() {
			return new SQLiteAsyncConnection (dbpath);
		}

		public static string dbpath
		{
			get 
			{ 
				var sqliteFilename = "tagdb.db3";
				#if NETFX_CORE
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);
				#else

				#if SILVERLIGHT
				// Windows Phone expects a local path, not absolute
				var path = sqliteFilename;
				#else

				#if __ANDROID__
				// Just use whatever directory SpecialFolder.Personal returns
				string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
				#else
				// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				// (they don't want non-user-generated data in Documents)
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
				#endif
				var path = Path.Combine (libraryPath, sqliteFilename);
				#endif

				#endif
				return path;	
			}
		}

		public async static Task<bool> ExistsUserInDatastore (string uid, DatastoreType type, SQLiteAsyncConnection db)
		{
			return (await db.Table<Datastore> ().Where (x => x.uid == uid && x.Type == type).CountAsync()) > 0;
		}

        public static Datastore GetDatastore(int id, SQLiteConnection db)
        {
            return db.Table<Datastore>().Where(x => x.Id == id).FirstOrDefault();
        }

        public async static Task<Datastore> GetDatastore (int id, SQLiteAsyncConnection db)
		{
			return await db.Table<Datastore> ().Where (x => x.Id == id).FirstOrDefaultAsync ();
		}

        public static List<Datastore> GetDatastores(SQLiteConnection db)
        {
            return db.Table<Datastore>().ToList();
        }

        public async static Task<IEnumerable<Datastore>> GetDatastores (SQLiteAsyncConnection db)
		{
			var table = await db.Table<Datastore> ().ToListAsync();
			return table;
		}

		public async static Task<IEnumerable<Archivo>> GetArchivos (SQLiteAsyncConnection db)
		{
			var table = await db.Table<Archivo> ().ToListAsync();
			return table;
		}

        public async static Task<List<Archivo>> GetArchivos(int datastore_id, SQLiteAsyncConnection db)
        {
            var table = await db.Table<Archivo>().Where(x => x.Datastore_id == datastore_id).ToListAsync();
            return table;
        }

        public async static Task<IEnumerable<Archivo>> GetArchivos (List<Tag> tagList, SQLiteAsyncConnection db, int init = 0, int length = 0)
		{
			try {
				bool withoutTags = (tagList == null || tagList.Count == 0);

				if (withoutTags) {
					if (length == 0)
						return await db.QueryAsync<Archivo> ("select * from archivo a where Id not in (select archivo_id from archivo_tag)");
					else
						return await db.QueryAsync<Archivo> ("select * from archivo a where Id not in (select archivo_id from archivo_tag) limit ?, ?", init, length);
				
				} else {
					var sqlb = new StringBuilder();
					if (length == 0)
						sqlb.AppendFormat("select a.* from archivo a where a.Id in (select archivo_id from archivo_tag where tag_id in ({0}) group by archivo_id having count(*) = {1} )", TagManager.TagListAsStringIds(tagList), tagList.Count() );
					else
						sqlb.AppendFormat(
							"select a.* from archivo a where a.Id in (select archivo_id from archivo_tag where tag_id in ({0}) group by archivo_id having count(*) = {1} ) limit {2}, {3}",
							TagManager.TagListAsStringIds(tagList),
							tagList.Count(),
							init,
							length
						);
					return await db.QueryAsync<Archivo> (sqlb.ToString());
				}
			} catch (Exception ex) {
				return null;
			}
//			var query =	from at in db.Table<Archivo_Tag>()
//						where tagList.Contains(
//						select at;
						

//			var apple = from s in db.Table<Stock>()
//					where s.Symbol.StartsWith ("A")
//				select s;
//			Console.WriteLine ("-> " + apple.FirstOrDefault ().Symbol);

		}



		public async static Task<Archivo> GetArchivo(int id, SQLiteAsyncConnection db) {
			return await db.Table<Archivo> ().Where (x => x.Id == id).FirstOrDefaultAsync ();
		}

		public async static Task<Archivo> GetArchivo(string datastoreFileId, SQLiteAsyncConnection db) {
			return await db.Table<Archivo> ().Where (x => x.DatastoreFile_id == datastoreFileId).FirstOrDefaultAsync ();
		}

		public static Archivo GetArchivo(int archivo_id, SQLiteConnection db) {
			return db.Table<Archivo> ().Where (x => x.Id == archivo_id).FirstOrDefault ();
		}

        public async static Task<Archivo> GetArchivoByMD5(string md5, int datastore_id, SQLiteAsyncConnection db)
        {
            return await db.Table<Archivo>().Where(x => x.MD5 == md5 && x.Datastore_id == datastore_id).FirstOrDefaultAsync();
        }

        public static Archivo GetArchivo(string datastoreFileId, SQLiteConnection db) {
			return db.Table<Archivo> ().Where (x => x.DatastoreFile_id == datastoreFileId).FirstOrDefault ();
		}

		public async static Task<Tag> GetTag(int tag_id, SQLiteAsyncConnection db) {
			return await db.Table<Tag> ().Where(x => x.Id == tag_id).FirstOrDefaultAsync();
		}

		public async static Task<List<Tag>> GetTags(string filterName, SQLiteAsyncConnection db) {
			return await db.Table<Tag> ().Where(x => x.Name.StartsWith(filterName)).OrderBy(x => x.Name).ToListAsync();
		}

		public async static Task<List<Tag>> GetArchivoTags(SQLiteAsyncConnection db, int archivo_id) {
			return await db.QueryAsync<Tag> ("select t.* from tag t, archivo_tag at where t.Id = at.tag_id and at.archivo_id = ? order by t.name", archivo_id);
		}

		public async static Task<List<Tag>> GetArchivoTags(SQLiteAsyncConnection db, List<Tag> tagList = null) {
			if (tagList == null) {
				return await db.QueryAsync<Tag> ("select distinct t.* from tag t, archivo_tag at where t.Id = at.tag_id order by t.name ");
			} else {
				var tagIdsAsString = TagManager.TagListAsStringIds (tagList);
				var sqlb = new StringBuilder();
				sqlb.AppendFormat(
					"select distinct t.* from tag t, archivo_tag at where t.Id = at.tag_id and at.archivo_id in " +
						"(select archivo_id from archivo_tag where tag_id in ({0}) group by archivo_id having count(*) = {1} ) " +
					"and t.Id not in ({0}) order by t.name", tagIdsAsString, tagList.Count() );
				return await db.QueryAsync<Tag> (sqlb.ToString());
				//return await db.QueryAsync<Tag> ("select distinct t.* from tag t, archivo_tag at where t.Id = at.tag_id");
			}
		}

		public async static Task<bool> AddTag(string tagname, int archivo_id, SQLiteAsyncConnection db) {
			try {
				tagname = tagname.ToLower();

				var taglist = await db.QueryAsync<Tag> ("select t.* from tag t, archivo_tag at where t.Id = at.tag_id and at.archivo_id = ? and t.name = ?", archivo_id, tagname);
				if (taglist.Count > 0) {
					// already exists
					return true;
				}
				else {
					var newtag = await db.Table<Tag>().Where(t => t.Name == tagname).FirstOrDefaultAsync();
					if (newtag == null) {
						newtag = new Tag { Name = tagname };
						var r = await db.InsertAsync(newtag);
					}

					var tg = new Archivo_Tag {
						Archivo_id = archivo_id,
						Tag_id = newtag.Id
					};
					var r2 = await db.InsertAsync(tg);

					RefreshTagDescription(archivo_id, db);

					return r2 > 0;
				}

			} catch (Exception) {
				return false;
			}
		}

//		public static bool AddTag(string tagname, int archivo_id, SQLiteConnection db) {
//			try {
//				tagname = tagname.ToLower();
//
//				var taglist = db.Query<Tag> ("select t.* from tag t, archivo_tag at where t.Id = at.tag_id and at.archivo_id = ? and t.name = ?", archivo_id, tagname);
//				if (taglist.Count > 0) {
//					// already exists
//					return true;
//				}
//				else {
//					var newtag = db.Table<Tag>().Where(t => t.Name == tagname).FirstOrDefault();
//					if (newtag == null) {
//						newtag = new Tag { Name = tagname };
//						var r = db.Insert(newtag);
//					}
//
//					var tg = new Archivo_Tag {
//						Archivo_id = archivo_id,
//						Tag_id = newtag.Id
//					};
//					var r2 = db.Insert(tg);
//
//					RefreshTagDescription(archivo_id, db);
//
//					return r2 > 0;
//				}
//
//			} catch (Exception) {
//				return false;
//			}
//		}

		public async static Task<bool> AddTag(int tag_id, int archivo_id, SQLiteAsyncConnection db) {
			try {

				var taglist = await db.QueryAsync<Archivo_Tag> ("select at.* from archivo_tag at where at.tag_id = ? and at.archivo_id = ?", tag_id, archivo_id);
				if (taglist.Count > 0) {
					// already exists
					return true;
				}
				else {
					var tg = new Archivo_Tag {
						Archivo_id = archivo_id,
						Tag_id = tag_id
					};
					var r2 = await db.InsertAsync(tg);

					RefreshTagDescription(archivo_id, db);

					return r2 > 0;
				}

			} catch (Exception) {
				return false;
			}
		}

		public async static Task<bool> RefreshTagDescription(int archivo_id, SQLiteAsyncConnection db) {
			try {
				var archivo = await DatabaseManager.GetArchivo(archivo_id, db);
				var archivoTagList = await ArchivoManager.GetArchivoTags (archivo_id, db);
				string desc = "[" + TagManager.TagListAsStringNames (archivoTagList) + "]";
				archivo.TagDescription = desc;
				await db.UpdateAsync(archivo);

				return true;

			} catch (Exception ex) {
				return ex.Message.Length == 0;
			}
		}

		public async static Task<bool> RemoveTag(int tag_id, int archivo_id, SQLiteAsyncConnection db) {
			try {

				var r = await db.ExecuteAsync("delete from archivo_tag where tag_id = ? and archivo_id = ?", tag_id, archivo_id);
				return r >= 0;

			} catch (Exception ex) {
				return false;
			}
		}

		public async static Task<bool> DeleteTag(int tag_id, SQLiteAsyncConnection db) {
			try {

				var r = await db.ExecuteAsync("delete from archivo_tag where tag_id = ?", tag_id);
				var r2 = await db.ExecuteAsync("delete from tag where id = ?", tag_id);
				return r >= 0 && r2 > 0;

			} catch (Exception ex) {
				var m = ex.Message;
				return false;
			}
		}


	}
}

