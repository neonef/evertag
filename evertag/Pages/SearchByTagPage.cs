﻿using System;

using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace evertag
{
	public class SearchByTagPage : ContentPage
	{
		ListView listview;
		SearchBar searchBar;
		Label resultsLabel;
		List<Tag> tagList = new List<Tag>();
		//ObservableCollection<Archivo> archivoList = new ObservableCollection<Archivo>();
		ObservableCollection<FolderPageItem> itemFileList = new ObservableCollection<FolderPageItem>();

		public SearchByTagPage ()
		{
			resultsLabel = new Label {
				Text = "Result will appear here.",
				VerticalOptions = LayoutOptions.FillAndExpand,
				FontSize = 25
			};

			searchBar = new SearchBar {
				Placeholder = "Enter search term",
				SearchCommand = new Command(() => {
					resultsLabel.Text = "Result: " + searchBar.Text + " is what you asked for.";
				})
			};

			listview = new ListView {
				ItemsSource = itemFileList,
				ItemTemplate = new DataTemplate (typeof(MyImageCell)),
				RowHeight = 80,
			};

			Content = new StackLayout { 
				Children = {
					searchBar,
					listview
				}
			};
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();
			await UpdateContent ();
		}

		async Task UpdateContent() {
			try {
				var db = DatabaseManager.GetNewAsyncConnection ();

				List<Archivo> archivoList;
				if (tagList == null || tagList.Count == 0)
					archivoList = new List<Archivo> (await DatabaseManager.GetArchivos(db));
				else
					archivoList = new List<Archivo> (await DatabaseManager.GetArchivos (tagList, db));

				foreach (var item in archivoList) {
					itemFileList.Add(new FolderPageItem {
						Id = item.Id,
						Name = item.FileName, 
						Type = FolderPageItemType.Archivo,
						archivo = item,
						Path = await ArchivoManager.GetThumbnailPath(item),
						Size = (ulong)item.Size
					});
				}
			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "Ok");
			}


		}
	}
}


