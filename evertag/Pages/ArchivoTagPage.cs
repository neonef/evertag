﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using FFImageLoading.Forms;

//https://www.iconfinder.com/search/?q=iconset%3Ahawcons+tag
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Linq;

namespace evertag
{
	public class ArchivoTagPage : ContentPage
	{
		Archivo archivo;
		List<Tag> taglist;
		List<Tag> archivoTagList;
		ObservableCollection<ArchivoTagPageItem> itemList = new ObservableCollection<ArchivoTagPageItem> ();
		Entry entry = new Entry {
            Placeholder = "Write a tag",
            TextColor = Color.Black,
        };
		ListView listview;
		Label tagText;
		bool firstShow = true;
		CachedImage cachedImage;

		public ArchivoPage2 ArchivoPage2 { get; set; }

		public ArchivoTagPage(int archivo_id)
		{
			BackgroundColor = Color.White;

            archivo = DatabaseManager.GetArchivo(archivo_id, DatabaseManager.GetNewConnection());

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

            Title = "Tags of " + archivo.FileName;

            entry.TextChanged += async (object sender, TextChangedEventArgs e) => {
				await RefreshItemList();
			};

			cachedImage = new CachedImage() {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				//WidthRequest = 50,
				HeightRequest = 70,
				//CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				LoadingPlaceholder = "loading.png",
				ErrorPlaceholder = "error.png",
				Source = ArchivoManager.GetThumbnailPath(archivo).Result,
			};

			tagText = new Label {
				Text = "",
                TextColor = Color.Black,
			};

			StackLayout imageBar = new StackLayout {
				Orientation = StackOrientation.Horizontal,
				Children = {
					cachedImage,
					new StackLayout {
						Orientation = StackOrientation.Vertical,
						Children = {
							new Label {
                                Text = archivo.FileName,
                                TextColor = Color.Black,
                            },
							tagText,
						}
					}
				}
			};

			listview = new ListView {
				ItemsSource = itemList,
				ItemTemplate = new DataTemplate(typeof(TagCell)),
			};
			listview.ItemTapped += OnItemTapped;


			Content = new StackLayout {
				Children = {
					imageBar,
					entry,
					listview,
				}
			};

			MessagingCenter.Subscribe<TagCell, int> (this, "DeleteTag", async (sender, arg) => {
				await RefreshTagList ();
				await RefreshArchivoTagList();
				await RefreshItemList();
			});
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			if (firstShow) {
				firstShow = false;
				var db = DatabaseManager.GetNewAsyncConnection ();
				await RefreshTagList ();
				await RefreshArchivoTagList ();
			}

			await RefreshItemList ();

		}

		protected async Task RefreshTagList () {
			taglist = await DatabaseManager.GetTags ("", DatabaseManager.GetNewAsyncConnection());
		}

		protected async Task RefreshArchivoTagList () {
			archivoTagList = await ArchivoManager.GetArchivoTags (archivo.Id, DatabaseManager.GetNewAsyncConnection());
		}

		async Task RefreshItemList () {
			try {
				//itemList.Clear ();
				
				//archivoTagList = await ArchivoManager.GetArchivoTags (archivo.Id, db);

				string pattern = entry.Text ?? "";
				pattern = pattern.Trim ().ToLower();
				if (!string.IsNullOrEmpty (pattern)) {
					pattern = TagManager.PatternNormalizator (pattern);
				}
				
				var filteredTagList = taglist.Where(x => x.Name.Contains(pattern)).ToList();

				itemList.Clear ();

				foreach (var item in filteredTagList) {
					bool assigned = archivoTagList.Exists (x => x.Id == item.Id);
					itemList.Add (new ArchivoTagPageItem {
						Id = item.Id,
						Name = item.Name,
						Assigned = assigned,
						Source = assigned ? "tag_fill_128.png" : "tag_empty_128.png",
						Type = ArchivoTagPageItemType.tag,
						archivoid_tagid = new int[2] {archivo.Id, item.Id}
					});
				}
				
				if (pattern.Length > 0 && itemList.Count == 0) {
					itemList.Insert (0, new ArchivoTagPageItem {
						Name = "Create \"" + pattern + "\"",
						Type = ArchivoTagPageItemType.create_tag,
						Source = "tag_add_128.png",
					});
				}

				tagText.Text = "[" + TagManager.TagListAsStringNames (archivoTagList) + "]";
			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "Ok");
			}
		}

		async void OnItemTapped (object sender, ItemTappedEventArgs e) {
			try {
				var db = DatabaseManager.GetNewAsyncConnection ();
				var item = e.Item as ArchivoTagPageItem;
				if (item.Type == ArchivoTagPageItemType.tag) {
					if (item.Assigned) {
						if (await DatabaseManager.RemoveTag(item.Id, archivo.Id, db)) {
							await RefreshArchivoTagList();
							await RefreshItemList();
						}
						else {
							await DisplayAlert("Error", "Tag not removed", "Ok");
						}
					}
					else {
						if (await DatabaseManager.AddTag(item.Id, archivo.Id, db)) {
							entry.Text = "";
							await RefreshArchivoTagList();
							await RefreshItemList();
						}
						else {
							await DisplayAlert("Error", "Tag not assigned", "Ok");
						}
					}
				} else if (item.Type == ArchivoTagPageItemType.create_tag) {

					// Create tag

					string finalName = item.Name.Substring(8, item.Name.Length - 9).ToLower();
					if (await DatabaseManager.AddTag(finalName, archivo.Id, db)) {
						entry.Text = "";
						await RefreshTagList();
						await RefreshArchivoTagList();
						await RefreshItemList();
					}
					else {
						await DisplayAlert("Error", "Tag not assigned", "Ok");
					}
				}

				//MessagingCenter.Send<TagCell, int> (this, "DeleteTag", tag_id);
				MessagingCenter.Send(this, "ArchivoTagRelChanged", archivo.Id);

				listview.SelectedItem = null;
			} catch (Exception ) {
				
			}
		}

	}
}

