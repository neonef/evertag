﻿using System;
using SQLite;

namespace evertag
{
	public class Datastore
	{
		[PrimaryKey, AutoIncrement, Column("_id")]
		public int Id { get; set; }

		[MaxLength(20)]
		public string Name { get; set; }

		public DatastoreType Type { get; set; }

		public string uid { get; set; }

		public string email { get; set; }

		public string AccessToken { get; set; }

		public string RefreshToken { get; set; }

		public DateTime TokenExpired { get; set; }

		public long FreeSpace { get; set; }

	}

	public enum DatastoreType {
		Dropbox, OneDrive, GoogleDrive, S3, LocalStorage
	}
}

