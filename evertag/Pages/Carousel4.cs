﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;
using System.Linq;

namespace evertag
{
	public class Carousel4 : CarouselPage
	{
		List<FolderPageItem> list;
		int delta = 3;
		int index;
		int start; 
		int end;
		bool loading = false;
        List<Datastore> datastores;


        public FolderPage folderPage { get; set; }

		public Carousel4 (List<FolderPageItem> list, FolderPageItem selected)
		{
            using (var db = DatabaseManager.GetNewConnection())
            {
                datastores = DatabaseManager.GetDatastores(db);
            }

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

            var padding = new Thickness (0, Device.OnPlatform (40, 40, 0), 0, 0);
			this.list = list;
			index = list.IndexOf (selected);
			start = index - delta;
			if (start < 0)
				start = 0;

			end = start + delta * 2;
			if (end > list.Count - 1)
				end = list.Count - 1;

			loading = true;
			for (int i = start; i <= end; i++) {
                var ds = datastores.FirstOrDefault(x => x.Id == list[i].archivo.Datastore_id);
                var page = new ArchivoPage2 (list [i], ds);
				page.carousel = this;
				Children.Add(page);
				if (i == index)
					CurrentPage = page;
			}
			loading = false;

			var cont = Children.Count;

			this.CurrentPageChanged += (object sender, EventArgs e) => {
				try {
					var page = CurrentPage as ArchivoPage2;
					//Title = page.Title;

					// nuevo
					if (loading) return;
					index = list.IndexOf(page.fpi);
					if (index == start) {
						start = start - delta;
						if (start < 0)
							start = 0;
						for (int i = index - 1; i >= start; i--) {
                            var ds = datastores.FirstOrDefault(x => x.Id == list[i].archivo.Datastore_id);
							var npage = new ArchivoPage2 (list [i], ds);
							page.carousel = this;
							Children.Insert(0, npage);
						}
//						if (end - start > delta * 3) {
//							int delini = end - delta;
//							for (int i = end; i >= delini; i--) {
//								Children.RemoveAt(i);
//							}
//						}
					}
					if (index == end) {
						end = end + delta;
						if (end > list.Count - 1)
							end = list.Count - 1;
						for (int i = index + 1; i <= end ; i++) {
                            var ds = datastores.FirstOrDefault(x => x.Id == list[i].archivo.Datastore_id);
                            var npage = new ArchivoPage2 (list [i], ds);
							page.carousel = this;
							Children.Add(npage);
						}
					}

				} catch (Exception ex) {
					var m = ex.Message;
				}
			};
		}

		protected override void OnCurrentPageChanged ()
		{
			base.OnCurrentPageChanged ();

			try {
				var page = CurrentPage as ArchivoPage2;
				Title = page.Title;

			} catch (Exception ex) {
				var m = ex.Message;
			}

		}

		public void DropPage(ContentPage page) {
			Children.Remove (page);
		}

		protected override bool OnBackButtonPressed ()
		{
			this.Children.Clear ();
			if (folderPage != null) {
				folderPage.UpdateContent ();
			}
			return base.OnBackButtonPressed ();
		}
	}
}


