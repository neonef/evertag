﻿using System;
using Xamarin.Forms;
using FFImageLoading.Forms;

namespace evertag
{
	public class MyImageCell : ViewCell
	{
		public CachedImage cachedImage = null;

		Label id;
		Label left;
        //Label right;

        public MyImageCell()
        {

            //instantiate each of our views
            cachedImage = new CachedImage() {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                WidthRequest = 100,
                HeightRequest = 100,
                DownsampleHeight = 100,
                DownsampleUseDipUnits = true,
                TransparencyEnabled = false,
                Aspect = Aspect.AspectFill,
                CacheDuration = TimeSpan.FromDays(30),
                RetryCount = 3,
                RetryDelay = 500,
                LoadingPlaceholder = "wait_128.png",
                ErrorPlaceholder = "document_128.png",
                //Source = "http://loremflickr.com/600/600/nature?filename=simple.jpg",
            };
            cachedImage.Error += async (object sender, CachedImageEvents.ErrorEventArgs e) => {
                try {
                    var db = DatabaseManager.GetNewAsyncConnection();
                    var ar = await DatabaseManager.GetArchivo(int.Parse(id.Text), db);
                    cachedImage.Source = await ArchivoManager.DownloadMiniThumbnail(ar, db);
                } catch (Exception ex) {
                    var ee = ex.Message;
                }
            };

            left = new Label {
                LineBreakMode = LineBreakMode.CharacterWrap,
                TextColor = Color.Black,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
            };

			id = new Label ();

            StackLayout horizontalLayout = new StackLayout();
            horizontalLayout.Orientation = StackOrientation.Horizontal;
            horizontalLayout.HorizontalOptions = LayoutOptions.FillAndExpand;
            horizontalLayout.VerticalOptions = LayoutOptions.FillAndExpand;
			
			horizontalLayout.Children.Add (cachedImage);
			horizontalLayout.Children.Add (left);
			View = horizontalLayout;
		}
        
        protected override void OnBindingContextChanged ()
		{
			base.OnBindingContextChanged ();

            try
            {
                //set bindings
                left.SetBinding(Label.TextProperty, "Name");
                //right.SetBinding (Label.TextProperty, "Size");
                id.SetBinding(Label.TextProperty, "Id");
                cachedImage.SetBinding(CachedImage.SourceProperty, "Path");
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }

		}
        
	}
}
