﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using FFImageLoading.Forms;

namespace evertag
{
	public partial class MasterPage : ContentPage
	{
		public ListView listView {
			get;
			set;
		}

		public ListView ListView { get { return listView; } }

		public MasterPage ()
		{
			var masterPageItems = new List<MasterPageItem> ();
//			masterPageItems.Add (new MasterPageItem {
//				Title = "Browse by Folder",
//				IconSource = "contacts.png",
//				TargetType = typeof(FolderPage)
//			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Files",
				IconSource = "documents_128.png",
				TargetType = typeof(TagPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Clouds",
				IconSource = "clouds_128.png",
				TargetType = typeof(TagPage)
			});
			masterPageItems.Add (new MasterPageItem {
				Title = "Sync",
				IconSource = "sync_cloud_128.png",
				TargetType = typeof(TagPage)
			});
			//masterPageItems.Add (new MasterPageItem {
			//	Title = "Reset Database",
			//	IconSource = "todo.png",
			//	TargetType = typeof(TagPage)
			//});

			listView = new ListView {
				ItemsSource = masterPageItems,
				BackgroundColor = Color.White,
				RowHeight = 70,
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.
						Label nameLabel = new Label {
                            LineBreakMode = LineBreakMode.CharacterWrap,
                            TextColor = Color.Black,
                            VerticalTextAlignment = TextAlignment.Center,
                            HorizontalTextAlignment = TextAlignment.End,
                        };
						nameLabel.SetBinding(Label.TextProperty, "Title");

						var cachedImage = new CachedImage() {
							HorizontalOptions = LayoutOptions.Center,
							VerticalOptions = LayoutOptions.Center,
                            Aspect = Aspect.AspectFill,
                            //WidthRequest = 50,
                            HeightRequest = 40,
							CacheDuration = TimeSpan.FromDays(30),
							DownsampleToViewSize = true,
							RetryCount = 0,
							RetryDelay = 250,
							TransparencyEnabled = false,
							LoadingPlaceholder = "loading.png",
							ErrorPlaceholder = "error.png",
						};
						cachedImage.SetBinding (CachedImage.SourceProperty, "IconSource");

						// Return an assembled ViewCell.
						return new ViewCell
						{
							View = new StackLayout
							{
								Padding = new Thickness(0, 5),
								Children = 
								{
									new StackLayout
									{
                                        Orientation = StackOrientation.Horizontal,
                                        HorizontalOptions = LayoutOptions.FillAndExpand,
                                        VerticalOptions = LayoutOptions.FillAndExpand,
										Spacing = 0,
										Children = 
										{
											cachedImage,
											nameLabel,
										}
									}
								}
							}
						};
					})
			};

			Title = "EverTAg";

			Content = new StackLayout {
				Children = {
					listView
				}
			};


		}
	}
}

