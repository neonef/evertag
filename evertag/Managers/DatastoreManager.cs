﻿using System;
using SQLite;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;

namespace evertag
{
	public class DatastoreManager
	{
		public static string[] PhotoExtensions = {"jpg", "jpeg", "png"};
		public static string[] DocumentExtensions = {"doc", "docx", "pdf", "xls", "xlsx", "ppt", "pptx",
			"odt", "odp", "ods", "odg", "tex", "word", "rtf"};
		public static string[] EBookExtensions = {"epub", "mobi"};
		public static string[] VideoExtensions = {"3gp", "avi", "m4v", "mkv", "mov", "mpeg", "mpg", "mpe", "mp4", "webm"};
		public static string[] AudioExtensions = {"mp3", "ogg", "m4a", "wav", "wma"};

        public static string[] AllExtensions = {"jpg", "jpeg", "doc", "docx", "pdf", "xls", "xlsx", "ppt", "pptx", "odt", "odp", "ods", "odg", "tex", "word", "rtf", "epub", "mobi", "3gp", "avi", "m4v", "mkv", "mov", "mpeg", "mpg", "mpe", "mp4", "webm", "mp3", "ogg", "m4a", "wav", "wma" };

        public DatastoreManager ()
		{
            
        }

		public static async Task SetTagByExtension(Archivo ar, SQLiteAsyncConnection db) {
			string extension = System.IO.Path.GetExtension(ar.FileName).ToLower().Replace(".", "");
			if (DocumentExtensions.Contains(extension)) {
				await DatabaseManager.AddTag("document", ar.Id, db);
			} else if (PhotoExtensions.Contains(extension)) {
				await DatabaseManager.AddTag("photo", ar.Id, db);
			} else if (EBookExtensions.Contains(extension)) {
				await DatabaseManager.AddTag("ebook", ar.Id, db);
			} else if (VideoExtensions.Contains(extension)) {
				await DatabaseManager.AddTag("video", ar.Id, db);
			} else if (AudioExtensions.Contains(extension)) {
				await DatabaseManager.AddTag("audio", ar.Id, db);
			}
		}

		public static async Task RefreshAll() {
			try {
				List<Datastore> dslist;
				var db = DatabaseManager.GetNewAsyncConnection();
				dslist = await db.Table<Datastore> ().ToListAsync();
				foreach (var ds in dslist) {
					if (ds.Type == DatastoreType.Dropbox) {
                        ////DropboxManager.Refresh2 (ds, "", true, true);
                        var dbm = new DropboxManager();
						await dbm.Refresh2 (ds, "", true, false);
					} else if (ds.Type == DatastoreType.GoogleDrive) {
                        var gdm = new GoogleDriveManager();
						await gdm.Refresh(ds);
					}

				}

			} catch (Exception ex) {
				var m = ex.Message;
			}
		
		}

        public static async Task<bool> Delete(int datastore_id, SQLiteAsyncConnection db)
        {
            try
            {
                var ds = await DatabaseManager.GetDatastore(datastore_id, db);
                if (ds == null)
                    return false;

                var archivos = await DatabaseManager.GetArchivos(datastore_id, db);
                if (archivos.Count == 0) {
                    return await db.DeleteAsync(ds) > 0;
                }

                var r = await db.ExecuteAsync("delete from archivo_tag where archivo_id in (select id from archivo where Datastore_id = ?)", datastore_id);

                var r2 = await db.ExecuteAsync("delete from archivo where Datastore_id = ?", datastore_id);

                var r3 = await db.DeleteAsync(ds);

                return true;

            }
            catch (Exception ex)
            {
                return ex.Message.Length == 0;
            }

        }
    }
}

