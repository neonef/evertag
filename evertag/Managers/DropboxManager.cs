﻿using System;
using System.Collections.Generic;
using Dropbox.Api;
using System.Threading.Tasks;
using System.Linq;
using SQLite;

namespace evertag
{
    public class DropboxManager : DatastoreManager
	{
		private static string MyAccessToken = "EfusKcuglx0AAAAAAAAPp-3UVpxG70XvsSabCeOIdr5_6rYdWUBMm1zuMX26b2zD";
		private static string AppKey = "elkj379xltzyau0";
		private static string ThumbnailUri = "https://content.dropboxapi.com/1/thumbnails/auto/{0}?access_token={1}&size={2}";

		public static async Task TestUser()
		{
			using (var dbx = new DropboxClient(MyAccessToken))
			{
				Dropbox.Api.Users.FullAccount full = await dbx.Users.GetCurrentAccountAsync();
				Console.WriteLine("{0} - {1}", full.Name.DisplayName, full.Email);
			}
		}

		public static DropboxClient GetNewDropboxClient(Datastore ds) {
			return new DropboxClient (ds.AccessToken);
		}

		public async static Task<DropboxClient> GetNewDropboxClient(int datastore_id, SQLiteAsyncConnection db) {
			var ds = await DatabaseManager.GetDatastore (datastore_id, db);
			return new DropboxClient (ds.AccessToken);
		}

		public static async Task<List<FolderPageItem>> ListFolder(Datastore ds, string path = "")
		{
			var items = new List<FolderPageItem>();

			using (var dbx = new DropboxClient(ds.AccessToken))
			{
				var list = await dbx.Files.ListFolderAsync(path);


				// show folders then files
				foreach (var item in list.Entries.Where(i => i.IsFolder))
				{
					Console.WriteLine("D  {0}/", item.Name);
					items.Add (new FolderPageItem {
						Name = item.Name,
						Type = FolderPageItemType.Folder,
						Datastore_id = ds.Id,
						Path = item.PathLower
					});
				}

				foreach (var item in list.Entries.Where(i => i.IsFile))
				{
					Console.WriteLine("F{0,8} {1}", item.AsFile.Size, item.Name);
					items.Add (new FolderPageItem {
						Name = item.Name,
						Size = item.AsFile.Size,
						Type = FolderPageItemType.Archivo,
						Datastore_id = ds.Id,
						Path = path
					});
				}
			}
			return items;
		}

		public static async Task<byte[]> Download (Datastore ds, string path, string filename)	{
			try {
				using (var dbx = new DropboxClient (ds.AccessToken)) {
					using (var response = await dbx.Files.DownloadAsync(path + "/" + filename))
					{
						return await response.GetContentAsByteArrayAsync ();
					}
				}
			} catch (Exception ex) {
				var e = ex.Message;
				return null;
			}
		}

		public static async Task<byte[]> Download (Datastore ds, string absolutepath)	{
			try {
				using (var dbx = new DropboxClient (ds.AccessToken)) {
					using (var response = await dbx.Files.DownloadAsync(absolutepath))
					{
						return await response.GetContentAsByteArrayAsync ();
					}
				}
			} catch (Exception ex) {
				var e = ex.Message;
				return null;
			}
		}

		public static async Task<byte[]> DownloadThumbnail (DropboxClient dbx, string absolutepath)	{
			try {
				using (var response = await dbx.Files.GetThumbnailAsync(absolutepath, null,
					Dropbox.Api.Files.ThumbnailSize.W640h480.Instance))
				{
					return await response.GetContentAsByteArrayAsync();
				}
			} catch (Exception) {
				return null;
			}
		}

		public static async Task<byte[]> DownloadMiniThumbnail (DropboxClient dbx, string absolutepath)	{
			try {
				using (var response = await dbx.Files.GetThumbnailAsync(absolutepath, null,
					Dropbox.Api.Files.ThumbnailSize.W128h128.Instance))
				{
					return await response.GetContentAsByteArrayAsync();
				}
			} catch (Exception) {
				return null;
			}
		}

		public static async Task<bool> DeleteFile (DropboxClient dbx, string absolutepath)	{
			try {
				var res = await dbx.Files.DeleteAsync(absolutepath);
				return true;

			} catch (Exception ex) {
				return ex.Message.Length == 0;
			}
		}

		public async Task Refresh2 (Datastore ds, string path = "", bool recursive = false, bool createThumnails = false) {
			try {
				using (var dbx = new DropboxClient (ds.AccessToken)) {
                    var list = await dbx.Files.ListFolderAsync(path, recursive, true);
                    int cont = 0;
                    int max = list.Entries.Where(i => i.IsFile).Count();

                    var db = DatabaseManager.GetNewAsyncConnection();
                    var archivos = await DatabaseManager.GetArchivos(ds.Id, db);

                    // Refresh space
                    try {
                        var sp = await dbx.Users.GetSpaceUsageAsync();
                        var free = sp.Allocation.AsIndividual.Value.Allocated - sp.Used;
                        var us = await dbx.Users.GetCurrentAccountAsync();

                        ds.FreeSpace = (long)free;
                        ds.Name = "Dropbox " + us.Name.DisplayName;

                        await db.UpdateAsync(ds);
                    }
                    catch (Exception ex)
                    {
                        var m = ex.Message;
                    }
                    


                    // files
                    foreach (var item in list.Entries.Where(i => i.IsFile))
					{
                        cont++;
                        var ar = archivos.Where(x => x.DatastoreFile_id == item.AsFile.Id).FirstOrDefault();
						if (ar == null) {
                            await Task.Delay(50);
                            ar = new Archivo {
								FileName = item.AsFile.Name,
								Size = (long)item.AsFile.Size,
								Datastore_id = ds.Id,
								DatastorePath = item.PathLower,
								DatastoreFile_id = item.AsFile.Id,
							};

							int res = await db.InsertAsync(ar);

							//ar.ThumbnailPath = await ArchivoManager.GetThumbnailPath(ar, createThumnails);
							ar.MiniThumbnailPath = await ArchivoManager.GetMiniThumbnailPath(ar, createThumnails);

//							if (createThumnails) {
//								var bytes = await DownloadThumbnail(dbx, item.PathLower);
//								if (bytes != null) {
//									ar.ThumbnailPath = ArchivoManager.SetThumbnail(ar, bytes);
//								}
//							}

							//https://content.dropboxapi.com/1/thumbnails/auto/{path}?access_token={token}&size=s
//							var sb = new System.Text.StringBuilder();
//							sb.AppendFormat(ThumbnailUri, item.PathLower, ds.AccessToken, "l");
//							ar.ThumbnailPath = sb.ToString();
//
//							sb.Clear();
//							sb.AppendFormat(ThumbnailUri, item.PathLower, ds.AccessToken, "m");
//							ar.MiniThumbnailPath = sb.ToString();




							// autotag by extension
							ar = await DatabaseManager.GetArchivo(item.AsFile.Id, db);
							await SetTagByExtension(ar, db);


                            if (cont % 10 == 0) {
                                Xamarin.Forms.MessagingCenter.Send<DropboxManager, string>(this, "SyncMessage", cont + " / " + max);
                            }
						}
                    
					}


                    
                    

                    //// folders
                    //if (recursive) {
                    //	foreach (var item in list.Entries.Where(i => i.IsFolder))
                    //	{
                    //		await Refresh2(ds, item.PathLower, recursive, createThumnails);
                    //	}
                    //}

                }
			} catch (Exception ex) {
				var m = ex.Message;
			}

		}


		public static string LoginUrl () {
			try {
				//http://dropbox.github.io/dropbox-sdk-dotnet/html/T_Dropbox_Api_DropboxOAuth2Helper.htm

				var oauth2State = Guid.NewGuid().ToString("N");
				//Uri authorizeUri = DropboxOAuth2Helper.GetAuthorizeUri(OAuthResponseType.Token, AppKey, "", state: oauth2State);
				Uri authorizeUri = DropboxOAuth2Helper.GetAuthorizeUri(OAuthResponseType.Token, AppKey, "https://www.dropbox.com/", oauth2State, false, true);

				//var authorizeUri = DropboxOAuth2Helper.GetAuthorizeUri(AppKey);
				return authorizeUri.ToString();
				//var r = DropboxOAuth2Helper.ProcessCodeFlowAsync(
			} catch (Exception ex) {
				var e = ex.Message;
				return e;
			}

		}

		public static async Task<bool> RegisterDatastoreInDB(OAuth2Response o2r) {
			try {
				if (o2r.State != null) {
					var uid = o2r.Uid;
					var db = DatabaseManager.GetNewAsyncConnection();
					if (await DatabaseManager.ExistsUserInDatastore(uid, DatastoreType.Dropbox, db)) {
						return true;
					} else {
						var ds = new Datastore {
							Name = uid,
							Type = DatastoreType.Dropbox,
							uid = uid,
							AccessToken = o2r.AccessToken,
							FreeSpace = 0
						};
						var insert = await db.InsertAsync(ds);

						return true;
					}
				}
				else {
					return false;
				}

			} catch (Exception ex) {
				var e = ex.Message;
				return false;
			}
		}

		public static OAuth2Response GetResponse(string redirectUrl) {
			try {
				return DropboxOAuth2Helper.ParseTokenFragment(new Uri(redirectUrl));
			} catch (Exception ) {
				return null;
			}
		}

		public static string LogoutUrl() {
			return "https://www.dropbox.com/logout";
		}

	}
}

