﻿using System;
using Xamarin.Forms;
using FFImageLoading.Forms;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace evertag
{
    public class ArchivoGridPage : ContentPage
	{
		Grid grid;
		Button editTags;
		Entry tagEntry;
		Label tagText;
		FolderPageItem fpi;
		Datastore ds;
		ListView tagListView;
		CachedImage  thumbnail;
		Archivo archivo;
		ObservableCollection<Tag> tagList = new ObservableCollection<Tag>();

		public ArchivoGridPage (FolderPageItem myfpi, Datastore myds)
		{
			this.fpi = myfpi;
			this.ds = myds;
			Title = myfpi.Name;

			grid = new Grid
			{
				VerticalOptions = LayoutOptions.FillAndExpand,
				RowDefinitions = 
				{
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					new RowDefinition { Height = GridLength.Auto },
					//new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
					//new RowDefinition { Height = new GridLength(100, GridUnitType.Absolute) }
				},
				ColumnDefinitions = 
				{
					new ColumnDefinition { Width = GridLength.Auto },
					new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
					new ColumnDefinition { Width = new GridLength(100, GridUnitType.Absolute) }
				}
			};

			grid.Children.Add(new Label
				{
					Text = myfpi.Name,
					FontSize = Device.GetNamedSize (NamedSize.Large, typeof(Label)),
					HorizontalOptions = LayoutOptions.Center
				}, 0, 3, 0, 1);

			tagText = new Label ();
			grid.Children.Add(tagText, 0, 3, 2, 3);

			editTags = new Button {
				Text = "Edit tags"
			};
			editTags.Clicked += OnEditTagButtonClicked;
			grid.Children.Add (editTags, 0, 3, 4, 5);


			// Accomodate iPhone status bar.
			this.Padding = new Thickness(10, Device.OnPlatform(20, 0, 0), 10, 5);

			// Build the page.
			this.Content = grid;
		}

		public async Task RefreshTagList(int archivo_id) {
			tagText.Text = "[" + TagManager.TagListAsStringNames (await ArchivoManager.GetArchivoTags (archivo_id, DatabaseManager.GetNewAsyncConnection())) + "]";
		}
//
		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			try {
				archivo = await DatabaseManager.GetArchivo (fpi.Id, DatabaseManager.GetNewAsyncConnection());

				var h = App.ScreenHeight / 3;

				thumbnail = new CachedImage { 
					//					Aspect = Aspect.AspectFit,
					//					DownsampleToViewSize = true,
					//					HeightRequest = h
				};
				thumbnail.Source = ImageSource.FromFile (await ArchivoManager.GetThumbnailPath (archivo, true));
				grid.Children.Add(thumbnail, 0, 3, 1, 2);


				await RefreshTagList(archivo.Id);


//				addButton = new Button {
//					Text = "+",
//				};
//				addButton.Clicked += OnAddButtonClicked;
//
//				tagEntry = new Entry {
//					Placeholder = "Add tag"	
//				};
//				tagEntry.Completed += OnTagEntryCompleted;
//				grid.Children.Add (tagEntry, 0, 2, 3, 4);
//				grid.Children.Add (addButton, 2, 3, 3, 4);

			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "Ok");
			}
		}

		async void OnAddButtonClicked (object sender, EventArgs e) {

			OnTagEntryCompleted (sender, e);
			//			button.Text += "-";
			//			var bytes = await DropboxManager.Download(
			//				ds,
			//				fpi.Path,
			//				fpi.Name);
			//			ArchivoManager.OpenFile(fpi.Name, bytes);
		}

		async void OnEditTagButtonClicked (object sender, EventArgs e) {
			await Navigation.PushAsync (new ArchivoTagPage(archivo.Id));
			await RefreshTagList(archivo.Id);
		}

		async void OnTagEntryCompleted(object sender, EventArgs e) {
			try {
				if (!string.IsNullOrWhiteSpace(tagEntry.Text.Trim())){
					if (await DatabaseManager.AddTag(tagEntry.Text.Trim(), archivo.Id, DatabaseManager.GetNewAsyncConnection())) {
						await RefreshTagList(archivo.Id);
						tagEntry.Text = "";
					}

				}

			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "OK");
			}
		}
	}
}

