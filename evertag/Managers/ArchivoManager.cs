﻿using System;
using System.IO;
using Android.Content;
using Android.Widget;
using System.Threading.Tasks;
using System.Collections.Generic;
using SQLite;
using PCLStorage;

namespace evertag
{
	public class ArchivoManager
	{
		public static List<string> ImageExtensions = new List<string> {".jpg", ".jpeg", ".png"};
        public static List<string> VideoExtensions = new List<string> { ".3gp", ".avi", ".m4v", ".mkv", ".mov", ".mpeg", ".mpg", ".mpe", ".mp4", ".webm" };

        public static string GetAppPath()
		{
			#if NETFX_CORE
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);
			#else

				#if SILVERLIGHT
					// Windows Phone expects a local path, not absolute
					var path = sqliteFilename;
					#else

						#if __ANDROID__
							// Just use whatever directory SpecialFolder.Personal returns
							string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
						#else
							// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
							// (they don't want non-user-generated data in Documents)
							string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
							string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
						#endif
						var path = libraryPath;
				#endif

			#endif
			return path;	
		}

		public static string GetPath(string filename)
		{
			#if NETFX_CORE
				var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, sqliteFilename);
			#else

				#if SILVERLIGHT
					// Windows Phone expects a local path, not absolute
					var path = sqliteFilename;
				#else

					#if __ANDROID__
						// Just use whatever directory SpecialFolder.Personal returns
						string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
					#else
						// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
						// (they don't want non-user-generated data in Documents)
						string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
						string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
					#endif
					var path = Path.Combine (libraryPath, filename);
				#endif

			#endif
			return path;	
		}

		public static string GetCachePath(string filename){
			var cachePath = Path.Combine (GetAppPath (), "Cache");
			if (!Directory.Exists (cachePath))
				Directory.CreateDirectory (cachePath);

			return Path.Combine (cachePath, filename);
		}

		public static async Task Open(Archivo ar, bool reload = false) {
			try {
                var db = DatabaseManager.GetNewAsyncConnection();
                if (reload)
                {
                    ar = await DatabaseManager.GetArchivo(ar.Id, db);
                }
				byte[] bytes = null;
				if (!string.IsNullOrEmpty(ar.LocalPath)) {
					// Already downloaded
					bytes = File.ReadAllBytes(ar.LocalPath);
				} else {
                    var ds = await DatabaseManager.GetDatastore(ar.Datastore_id, db);
					if (ds.Type == DatastoreType.Dropbox) {
						bytes = await DropboxManager.Download(ds, ar.DatastorePath);
					} else if (ds.Type == DatastoreType.GoogleDrive) {
						bytes = await GoogleDriveManager.Download(ds, ar);
					}
                    if (bytes != null)
                    {
                        var localPath = CalculateLocalPath(ar);
                        File.WriteAllBytes(localPath, bytes);
                        ar.LocalPath = localPath;
                        await db.UpdateAsync(ar);
                    }
				}
				OpenFile(ar.FileName, bytes);


			} catch (Exception ex) {
				var r = ex.Message;
			}
		}

		public static async Task<bool> Delete(Datastore ds, Archivo ar, SQLiteAsyncConnection db) {
			try {
				bool r1 = false;
				if (ds == null) {
					ds = await DatabaseManager.GetDatastore(ar.Datastore_id, db);
				}
                if (ds.Type == DatastoreType.Dropbox)
                {
                    r1 = await DropboxManager.DeleteFile(
                        await DropboxManager.GetNewDropboxClient(ar.Datastore_id, db),
                        ar.DatastorePath);
                }
                else if (ds.Type == DatastoreType.GoogleDrive)
                {
                    r1 = await GoogleDriveManager.DeleteFile(ds, ar);
                }
                else if (ds.Type == DatastoreType.LocalStorage)
                {
                    File.Delete(ar.LocalPath);
                    r1 = !File.Exists(ar.LocalPath);
                }

				if (r1) {
					var r2 = await db.ExecuteAsync("delete from archivo_tag where archivo_id = ?", ar.Id);
					var r3 = await db.ExecuteAsync("delete from archivo where id = ?", ar.Id);	
				}

				return true;
			} catch (Exception ex) {
				return ex.Message.Length == 0;
			}

		}

		public static async Task<bool> Disable(Archivo ar, SQLiteAsyncConnection db) {
			try {
				ar.State = ArchivoState.Disable;
				var res = await db.UpdateAsync(ar);
				return res > 0;

			} catch (Exception ex) {
				return ex.Message.Length == 0;
			}

		}

		public static void OpenFile(string filename, byte[] bytes)
		{

			//var bytes = File.ReadAllBytes(filePath);

			//Copy the private file's data to the EXTERNAL PUBLIC location
			string externalStorageState = global::Android.OS.Environment.ExternalStorageState;
			string application = "";

			string extension = System.IO.Path.GetExtension (filename);

			switch (extension.ToLower()) {
			case ".doc":
			case ".docx":
				application = "application/msword";
				break;
			case ".pdf":
				application = "application/pdf";
				break;
			case ".xls":
			case ".xlsx":
				application = "application/vnd.ms-excel";
				break;
			case ".jpg":
			case ".jpeg":
			case ".png":
				application = "image/jpeg";
				break;
			default:
				application = "*/*";
				break;
			}
//			var externalPath = global::Android.OS.Environment.ExternalStorageDirectory.Path + "/report" + extension;
//			File.WriteAllBytes(externalPath, bytes);

			try
			{
				var externalPath = global::Android.OS.Environment.ExternalStorageDirectory.Path + "/evertag";
				if (!Directory.Exists(externalPath))
					Directory.CreateDirectory(externalPath);
				externalPath = Path.Combine(externalPath, filename);
				File.WriteAllBytes(externalPath, bytes);

				Java.IO.File file = new Java.IO.File(externalPath); 
				file.SetReadable(true);
				//Android.Net.Uri uri = Android.Net.Uri.Parse("file://" + filePath);
				Android.Net.Uri uri =  Android.Net.Uri.FromFile(file);
				Intent intent = new Intent(Intent.ActionView);
				intent.SetDataAndType(uri, application);
				intent.SetFlags(ActivityFlags.ClearWhenTaskReset | ActivityFlags.NewTask);


				Xamarin.Forms.Forms.Context.StartActivity(intent);
			}
			catch (Exception ex)
			{
				Toast.MakeText(Xamarin.Forms.Forms.Context, "Error: " + ex.Message, ToastLength.Long).Show();
			}
		}

		public static async Task<byte[]> GetBinaryArchivo(Archivo ar) {
			try {
				if (!string.IsNullOrEmpty(ar.LocalPath)) {
					return File.ReadAllBytes(ar.LocalPath);
				}
				else {
					byte[] bytes = null;
					var ds = await DatabaseManager.GetDatastore(ar.Datastore_id, DatabaseManager.GetNewAsyncConnection());
					if (ds.Type == DatastoreType.Dropbox) {
						bytes = await DropboxManager.Download(ds, ar.DatastorePath);
					} else if (ds.Type == DatastoreType.GoogleDrive) {
						bytes = await GoogleDriveManager.Download(ds, ar);
					}

					if (bytes.Length > 0) {
						var cachepath = GetCachePath(Path.GetFileNameWithoutExtension(ar.FileName) + "_id:" + ar.Id + "_" + Path.GetExtension(ar.FileName).ToLower());
						ar.LocalPath = cachepath;

						var db = DatabaseManager.GetNewAsyncConnection();
						var r = await db.UpdateAsync(ar);
						File.WriteAllBytes(cachepath, bytes);
					}
					return bytes;
				}

			} catch (Exception) {
				return null;
			}
		}

		public static async Task<string> GetThumbnailPath(Archivo ar, bool create = false) {
			try {
                if (!string.IsNullOrEmpty(ar.ThumbnailPath))
                {
                    return ar.ThumbnailPath;
                }
                else
                {
                    string extension = Path.GetExtension(ar.FileName).ToLower();
                    if (extension == ".pdf")
                    {
                        return "pdf_128.png";
                    }
                    else if (extension == ".epub")
                    {
                        return "epub.png";
                    }
                    else if (ImageExtensions.Contains(extension))
                    {
                        if (!string.IsNullOrEmpty(ar.LocalPath))
                        {
                            return ar.LocalPath;
                        }

                        var thumbpath = CalculateThumbnailPath(ar);

                        if (create)
                        {
                            // Creating thumbnail
                            var db = DatabaseManager.GetNewAsyncConnection();

                            if (!string.IsNullOrEmpty(ar.LocalPath))
                            {
                                var bytes = await GetBinaryArchivo(ar);
                                var res = ImageResizer.ResizeImage(bytes, 1024, 768);
                                File.WriteAllBytes(thumbpath, bytes);

                                ar = await db.FindAsync<Archivo>(ar.Id);
                                ar.ThumbnailPath = thumbpath;
                                await db.UpdateAsync(ar);
                            }
                            else
                            {
                                await DownloadThumbnail(ar, db);
                            }

                            return thumbpath;
                        }
                        else
                        {
                            //return "document_128.png";
                            return thumbpath;
                        }
                    }
                    else if (VideoExtensions.Contains(extension))
                    {
                        if (create)
                        {
                            var path = ArchivoManager.CalculateThumbnailPath(ar);
                            var bitmap = await Android.Media.ThumbnailUtils.CreateVideoThumbnailAsync(ar.LocalPath, Android.Provider.ThumbnailKind.MiniKind);
                            using (MemoryStream ms = new MemoryStream())
                            {
                                await bitmap.CompressAsync(Android.Graphics.Bitmap.CompressFormat.Jpeg, 100, ms);
                                System.IO.File.WriteAllBytes(path, ms.ToArray());
                            }

                            ar.ThumbnailPath = path;
                            var db = DatabaseManager.GetNewAsyncConnection();
                            await db.UpdateAsync(ar);

                            return path;
                        }
                        return "";
                    }
                    else
                    {
                        return "document_128.png";
                    }
                }
				
			} catch (Exception ex) {
				var e = ex.Message;
				return null;
			}
		}

		public static async Task<string> GetMiniThumbnailPath(Archivo ar, bool create = false) {
			try {
                if (!string.IsNullOrEmpty(ar.MiniThumbnailPath))
                {
                    return ar.MiniThumbnailPath;
                }
                else
                {
                    string extension = Path.GetExtension(ar.FileName).ToLower();
                    if (extension == ".pdf")
                    {
                        return "pdf_128.png";
                    }
                    else if (extension == ".epub")
                    {
                        return "epub.png";
                    }
                    else if (extension == ".mp4") {
                        return "movie_128.png";
                    }
                    else if (ImageExtensions.Contains(extension))
                    {
                        if (!string.IsNullOrEmpty(ar.LocalPath))
                        {
                            return ar.LocalPath;
                        }

                        var thumbpath = CalculateMiniThumbnailPath(ar);

                        if (create)
                        {
                            // Creating mini thumbnail
                            var db = DatabaseManager.GetNewAsyncConnection();

                            if (!string.IsNullOrEmpty(ar.LocalPath))
                            {
                                var bytes = await GetBinaryArchivo(ar);
                                var res = ImageResizer.ResizeImage(bytes, 640, 480);
                                File.WriteAllBytes(thumbpath, bytes);

                                ar = await db.FindAsync<Archivo>(ar.Id);
                                ar.MiniThumbnailPath = thumbpath;
                                await db.UpdateAsync(ar);
                            }
                            else
                            {
                                await DownloadMiniThumbnail(ar, db);
                            }

                            return thumbpath;
                        }
                        else
                        {
                            //return "document_128.png";
                            return thumbpath;
                        }
                    }
                    else
                    {
                        return "document_128.png";
                    }
                }

			} catch (Exception ex) {
				var e = ex.Message;
				return null;
			}
		}

		public static async Task<string> DownloadThumbnail(Archivo ar, SQLiteAsyncConnection db){
			try {
				string extension = Path.GetExtension (ar.FileName).ToLower ();
				var thumbpath = GetCachePath (Path.GetFileNameWithoutExtension (ar.FileName) + "_t" + extension);
				
				var bytes = await DropboxManager.DownloadThumbnail (
					            await DropboxManager.GetNewDropboxClient (ar.Datastore_id, db),
					            ar.DatastorePath);
				
				if (bytes != null) {
					File.WriteAllBytes (thumbpath, bytes);
					ar.ThumbnailPath = thumbpath;
				
					//var thumbpath = GetCachePath(Path.GetFileNameWithoutExtension(ar.FileName) + "_t" + extension);
					//var res = ImageResizer.ResizeImage(await GetBinaryArchivo(ar), 1024, 1024);
					//File.WriteAllBytes(thumbpath, bytes);
				
					//ar = await db.FindAsync<Archivo>(ar.Id);
					//ar.ThumbnailPath = thumbpath;
				
					var r = await db.UpdateAsync (ar);
					return thumbpath;
				} else {
					return "document_128.png";
				}
			} catch (Exception ex) {
				var e = ex.Message;
				return "document_128.png";
			}
		}

		public static async Task<string> DownloadMiniThumbnail(Archivo ar, SQLiteAsyncConnection db){
			try {
				string extension = Path.GetExtension (ar.FileName).ToLower ();
				var thumbpath = GetCachePath (Path.GetFileNameWithoutExtension (ar.FileName) + "_mt" + extension);

				var bytes = await DropboxManager.DownloadMiniThumbnail (
					await DropboxManager.GetNewDropboxClient (ar.Datastore_id, db),
					ar.DatastorePath);

				if (bytes != null) {
					File.WriteAllBytes (thumbpath, bytes);
					ar.MiniThumbnailPath = thumbpath;

					var r = await db.UpdateAsync (ar);
					return thumbpath;
				} else {
					return "document_128.png";
				}
			} catch (Exception ex) {
				var e = ex.Message;
				return "document_128.png";
			}
		}

		public static string SetThumbnail(Archivo ar, byte[] bytes){
			try {
				string extension = Path.GetExtension(ar.FileName).ToLower();
				var thumbpath = GetCachePath(Path.GetFileNameWithoutExtension(ar.FileName) + "_t" + extension);
				File.WriteAllBytes(thumbpath, bytes);

				return thumbpath;
			} catch (Exception ex) {
				return null;
			}
		}

		public static async Task<List<Tag>> GetArchivoTags(int archivo_id, SQLiteAsyncConnection db) {
			try {
				var tags = await DatabaseManager.GetArchivoTags(db, archivo_id);
				return tags ?? new List<Tag>();
				
			} catch (Exception ex) {
				return new List<Tag>();
			}
		}

		public static async Task<List<Tag>> AddTag(string tagname, int archivo_id) {
			try {
				
				var tags = await DatabaseManager.GetArchivoTags(DatabaseManager.GetNewAsyncConnection(), archivo_id);
				return tags ?? new List<Tag>();

			} catch (Exception ex) {
				return new List<Tag>();
			}
		}

        public static string CalculateLocalPath(Archivo ar)
        {
            return GetCachePath("archivo_id:" + ar.Id + Path.GetExtension(ar.FileName).ToLower());
        }

        public static string CalculateThumbnailPath(Archivo ar)
        {
            return GetCachePath("thumbnail_id:" + ar.Id + Path.GetExtension(ar.FileName).ToLower());
        }

        public static string CalculateMiniThumbnailPath(Archivo ar)
        {
            return GetCachePath("minithumbnail_id:" + ar.Id + Path.GetExtension(ar.FileName).ToLower());
        }

        public static async Task GenerateThumbnail(Archivo ar, SQLiteAsyncConnection db)
        {
            try
            {
                var path = CalculateThumbnailPath(ar);

                var bytes = await GetBinaryArchivo(ar);
                var res = ImageResizer.ResizeImage(bytes, 1024, 768);
                File.WriteAllBytes(path, bytes);

                ar.ThumbnailPath = path;
                await db.UpdateAsync(ar);
            }
            catch (Exception)
            {

            }
        }

        public static async Task GenerateMiniThumbnail(Archivo ar, SQLiteAsyncConnection db) {
            try
            {
                var path = CalculateMiniThumbnailPath(ar);

                var bytes = await GetBinaryArchivo(ar);
                var res = ImageResizer.ResizeImage(bytes, 640, 480);
                File.WriteAllBytes(path, bytes);
            
                ar.MiniThumbnailPath = path;
                await db.UpdateAsync(ar);
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }
        }

        public static string HumanSize(double bytes) {
            string[] type = { "", "K", "M", "G", "T", "P", "exa", "zetta", "yotta" };
            int i = 0;
            while (bytes >= 1024) {
                bytes /= 1024;
                i++;
            }
            return Math.Round(bytes, 2) + " " + type[i];
        }

	}
}

