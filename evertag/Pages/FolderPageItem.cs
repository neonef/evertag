﻿namespace evertag
{
    public class FolderPageItem
	{
		public int Id {
			get;
			set;
		}
		public string Name {
			get;
			set;
		}
		public ulong Size {
			get;
			set;
		}
		public FolderPageItemType Type {
			get;
			set;
		}
		public string Path {
			get;
			set;
		}
		public int Datastore_id {
			get;
			set;
		}

		public Archivo archivo {
			get;
			set;
		}

		public Tag tag {
			get;
			set;
		}
	}

	public enum FolderPageItemType {
		Folder, Archivo, Tag, Datastore, Back, Text, Untagged, Next, Prev
	}
}

