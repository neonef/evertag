﻿using Xamarin.Forms;

namespace evertag
{
    public class TitleWebView : Xamarin.Forms.WebView
    {
        public static readonly BindableProperty PageTitleProperty = BindableProperty.Create<TitleWebView, string>(v => v.PageTitle, null, BindingMode.OneWayToSource);

        public string PageTitle
        {
            get { return (string)GetValue(PageTitleProperty); }
            set { SetValue(PageTitleProperty, value); }
        }
    }
}

