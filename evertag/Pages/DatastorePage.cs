﻿using System;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace evertag
{
    public class DatastorePage : ContentPage
	{
		ListView listview;
		ObservableCollection<DatastorePageItem> itemList = new ObservableCollection<DatastorePageItem> ();
        private int exitCount = 0;

        public DatastorePage ()
		{
			Title = "Datastores";

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

            listview = new ListView {
				ItemsSource = itemList,
                RowHeight = 110,
                ItemTemplate = new DataTemplate(typeof(DatastoreCell))
			};
            listview.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) =>
            {
                var item = e.SelectedItem as DatastorePageItem;
                if (item != null)
                {
                    if (item.Type == "New")
                    {
                        await Navigation.PushAsync(new AddDatastorePage());
                    }
                }
            };


            Content = new StackLayout { 
				Children = {
					listview,
				}
			};

            BackgroundColor = Color.White;

            MessagingCenter.Subscribe<DatastoreCell, int>(this, "DeleteDatastore", async (sender, arg) => {
                bool delete = await DisplayAlert(
                    "Delete a datastore",
                    "Are you sure to delete the datastore and all its files?",
                    "Yes",
                    "No"
                );
                if (delete)
                {
                    var db = DatabaseManager.GetNewAsyncConnection();
                    int datastore_id = arg;
                    var ds = await DatabaseManager.GetDatastore(datastore_id, db);
                    if (ds != null)
                    {
                        var result = await DatastoreManager.Delete(datastore_id, db);
                        if (result)
                        {
                            Android.Widget.Toast.MakeText(Forms.Context, "Datastore '" + ds.Name + "' deleted", Android.Widget.ToastLength.Short).Show();

                        }
                    }
                }


                OnAppearing();
            });
        }

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();
			var listaDS = await DatabaseManager.GetDatastores (DatabaseManager.GetNewAsyncConnection ());
            long totalFreeSpace = 0;
            exitCount = 0;

			itemList.Clear ();
			foreach (var item in listaDS) {
                string source = "";
                if (item.Type == DatastoreType.Dropbox)
                    source = "dropbox_200.png";
                else if (item.Type == DatastoreType.GoogleDrive)
                    source = "gdrive_512.png";
                else if (item.Type == DatastoreType.LocalStorage)
                    source = "phone_128_b.png";

                itemList.Add (new DatastorePageItem {
					 Name = item.Name + " (" + ArchivoManager.HumanSize(item.FreeSpace) + " free)",
                     Source = source,
                     Type = item.Type.ToString(),
                     Datastore_id = item.Id,
				});

                totalFreeSpace += item.FreeSpace;
			}

            itemList.Add(new DatastorePageItem
            {
                Datastore_id = -1,
                Name = "Add new cloud",
                Source = "add_cloud_512.png",
                Type = "New",
            });


        }

        protected override bool OnBackButtonPressed()
        {
            if (exitCount == 1)
            {
                exitCount = 0;
                return false;
            }
            else
            {
                Android.Widget.Toast.MakeText(Forms.Context, "Press Back again to quit", Android.Widget.ToastLength.Short).Show();
                exitCount++;
            }
            return true;
        }
    }

    public class DatastorePageItem {
        public string Name { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }
        public int Datastore_id { get; set; }
    }
}


