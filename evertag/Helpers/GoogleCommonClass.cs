﻿using System;
using System.Threading.Tasks;
using Android.App;
using Newtonsoft.Json;
using Xamarin.Forms;
using Android.Widget;
using System.Net;

namespace evertag
{
	public class GoogleCommonClass
	{

		static GoogleInfo googleInfo;
		static ProgressDialog progress;
		const string googUesrInfoAccessleUrl = "https://www.googleapis.com/oauth2/v1/userinfo?access_token={0}";  
		public static async Task<bool> fnGetProfileInfoFromGoogle(string access_token)
		{ 
			progress =  ProgressDialog.Show (Forms.Context,"","Please wait..."); 
			bool isValid=false;
			//Google API REST request
			string userInfo = await fnDownloadString (string.Format(googUesrInfoAccessleUrl, access_token ));  
			if ( userInfo != "Exception" )
			{ 
				//step 4: Deserialize the JSON response to get data in class object
				googleInfo = JsonConvert.DeserializeObject<GoogleInfo> ( userInfo );   
				isValid = true;

				// Registering datastore
				var db = DatabaseManager.GetNewAsyncConnection();
				var uid = googleInfo.id;
				if (!await DatabaseManager.ExistsUserInDatastore(uid, DatastoreType.GoogleDrive, db)) {
					var ds = new Datastore {
						Name = "Google Drive " + googleInfo.email,
						Type = DatastoreType.GoogleDrive,
						uid = uid,
						email = googleInfo.email,
						AccessToken = access_token,
						FreeSpace = 0
					};
					var insert = await db.InsertAsync(ds);
				}
			}
			else
			{ 
				if ( progress != null )
				{
					progress.Dismiss ();
					progress = null;
				}   
				isValid = false;
				Toast.MakeText (Forms.Context , "Connection failed! Please try again" , ToastLength.Short ).Show (); 
			}
			if ( progress != null )
			{
				progress.Dismiss ();
				progress = null;
			}  
			return isValid;
		}

		public static async Task<string> fnDownloadString(string strUri)
		{ 
			var webclient = new WebClient ();
			string strResultData;
			try
			{
				strResultData= await webclient.DownloadStringTaskAsync (new Uri(strUri));
				Console.WriteLine(strResultData);
			}
			catch
			{
				strResultData = "Exception";
//				RunOnUiThread ( () =>
//					Toast.MakeText ( this , "Unable to connect to server!!!" , ToastLength.Short ).Show());
			}
			finally
			{
				if ( webclient!=null )
				{
					webclient.Dispose ();
					webclient = null; 
				}
			}
			return strResultData;
		}
	}
}

