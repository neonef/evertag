﻿using System;
using SQLite;

namespace evertag
{
	public class Archivo
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		public string FileName { get; set; }

		public long Size { get; set; }

		public int Datastore_id { get; set; }

		public string DatastorePath { get; set; }

		public string DatastoreFile_id { get; set; }

		public string LocalPath { get; set; }

		public int ThumbnailSize { get; set; }

		public DateTime ThumbnailLastAccessed { get; set; }

		public string ThumbnailPath { get; set; }

		public string MiniThumbnailPath { get; set; }

		public string TagDescription { get; set; }

		public ArchivoState State { get; set; }

        public string MD5 { get; set; }

    }

	public enum ArchivoState {
		Enable, Disable
	}
}

