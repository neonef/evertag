﻿using System;
using System.Collections.Generic;
using System.Text;

namespace evertag
{
	public class TagManager
	{
		public static string TagListAsStringIds(List<Tag> list) {
			var tag_ids = new StringBuilder ();
			foreach (var tag in list) {
				tag_ids.Append (tag.Id + ",");
			}
			tag_ids.Remove (tag_ids.Length - 1, 1);
			return tag_ids.ToString ();
		}

		public static string TagListAsStringNames(List<Tag> list) {
			if (list == null || list.Count == 0)
				return "";

			var tag_ids = new StringBuilder ();
			foreach (var tag in list) {
				tag_ids.Append (tag.Name + ", ");
			}
			tag_ids.Remove (tag_ids.Length - 2, 2);
			return tag_ids.ToString ();
		}

		public static string PatternNormalizator (string pattern) {
			try {
				pattern = pattern.Trim ();
				var s = pattern.Split (' ');
				if (s.Length == 1) {
					return s [0];
				}
				var sb = new StringBuilder ();
				foreach (string item in s) {
					if (item != "") {
						sb.Append (item + "_");
					}
				}
				var r = sb.ToString (0, sb.Length - 1);
				return r.ToLower ();
			} catch (Exception ex) {
				return pattern;
			}

		}

	}
}

