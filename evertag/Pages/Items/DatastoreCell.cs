﻿using System;
using Xamarin.Forms;
using FFImageLoading.Forms;

namespace evertag
{
    public class DatastoreCell : ViewCell
	{
		public CachedImage cachedImage = null;

		public DatastoreCell ()
		{
			cachedImage = new CachedImage() {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
                WidthRequest = 100,
                HeightRequest = 100,
                DownsampleHeight = 100,
                DownsampleUseDipUnits = true,
                TransparencyEnabled = false,
                Aspect = Aspect.AspectFill,
                CacheDuration = TimeSpan.FromDays(30),
                RetryCount = 3,
                RetryDelay = 500,
                LoadingPlaceholder = "loading.png",
				ErrorPlaceholder = "error.png",
				//Source = "tag_128.png",
			};

			cachedImage.SetBinding (CachedImage.SourceProperty, "Source");

			StackLayout horizontalLayout = new StackLayout() {
                Orientation = StackOrientation.Horizontal,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };
            StackLayout verticalLayout = new StackLayout() {
                Orientation = StackOrientation.Vertical,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };

            Label Name = new Label {
                LineBreakMode = LineBreakMode.CharacterWrap,
                TextColor = Color.Black,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
            };
            Label Type = new Label {
                LineBreakMode = LineBreakMode.CharacterWrap,
                TextColor = Color.Black,
                VerticalTextAlignment = TextAlignment.Center,
                HorizontalTextAlignment = TextAlignment.Center,
            };

            Name.SetBinding (Label.TextProperty, "Name");
			Name.VerticalOptions = LayoutOptions.Center;

            Type.SetBinding(Label.TextProperty, "Type");
            Type.VerticalOptions = LayoutOptions.Center;

            verticalLayout.Children.Add(Type);
            verticalLayout.Children.Add(Name);

            horizontalLayout.Children.Add(cachedImage);
            horizontalLayout.Children.Add(verticalLayout);

            View = horizontalLayout;

            // Menu
            var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true }; // red background
            deleteAction.SetBinding(MenuItem.CommandParameterProperty, "Datastore_id");
            deleteAction.Clicked += (sender, e) =>
            {
                var mi = ((MenuItem)sender);
                var datastore_id = (int)mi.CommandParameter;

                if (datastore_id != -1)
                {
                    MessagingCenter.Send<DatastoreCell, int>(this, "DeleteDatastore", datastore_id);
                }
            };

            ContextActions.Add(deleteAction);

        }
    }
}

