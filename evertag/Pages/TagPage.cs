using System.Collections.Generic;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace evertag
{
    public class TagPage : ContentPage
	{
		ListView listview;
		Label label;

		public TagPage () {
			Content = new ScrollView {
				Content = new Label {
                    Text = "Tag page",
                    TextColor = Color.Black,
                }
			};
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();
			await SetContent ();
		}

		async Task SetContent() {

			List<FolderPageItem> itemsSource = new List<FolderPageItem> ();
			string labelText = "";

			var list = await DatabaseManager.GetArchivos (DatabaseManager.GetNewAsyncConnection());
			foreach (var item in list) {
				itemsSource.Add (new FolderPageItem {
					Id = item.Id,
					Name = item.FileName, 
					Type = FolderPageItemType.Datastore,
					Path = ""
				});
			}
			labelText = itemsSource.Count + " files found.";
				


			label = new Label { Text = labelText };
			listview = new ListView {
				ItemsSource = itemsSource,
				ItemTemplate = new DataTemplate(() =>
					{
						// Create views with bindings for displaying each property.
						Label nameLabel = new Label {
                            TextColor = Color.Black,
                        };
						nameLabel.SetBinding(Label.TextProperty, "Name");

						// Return an assembled ViewCell.
						return new ViewCell
						{
							View = new StackLayout
							{
								Padding = new Thickness(0, 5),
								Orientation = StackOrientation.Horizontal,
								Children = 
								{
									new StackLayout
									{
										VerticalOptions = LayoutOptions.Center,
										Spacing = 0,
										Children = 
										{
											nameLabel,
										}
									}
								}
							}
						};
					})

			};

			//listview.ItemSelected += OnItemSelected;

			Content = new StackLayout {
				Children = {
					label,
					listview
				}
			};

		}

	}


}

