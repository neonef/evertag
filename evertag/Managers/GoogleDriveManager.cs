﻿using System;
using System.Threading.Tasks;
using System.Linq;

namespace evertag
{
    public class GoogleDriveManager : DatastoreManager
	{
		public static async Task<bool> RegisterDatastoreInDB(string code) {
			try {
				var tokenResponse = await GoogleDriveMyREST.GetToken(code);
				if (tokenResponse != null) {
					var info = await GoogleDriveMyREST.GetAccountInfo(tokenResponse.access_token);
					if (info != null) {
						var db = DatabaseManager.GetNewAsyncConnection();
						var uid = info.user.emailAddress;
						if (await DatabaseManager.ExistsUserInDatastore(uid, DatastoreType.GoogleDrive, db)) {
							return true;
						} else {
							var ds = new Datastore {
								Name = uid,
								Type = DatastoreType.GoogleDrive,
								uid = uid,
								email = info.user.emailAddress,
								AccessToken = tokenResponse.access_token,
								RefreshToken = tokenResponse.refresh_token,
								TokenExpired = DateTime.Now.AddSeconds(tokenResponse.expires_in),
								FreeSpace = info.quotaBytesTotal - info.quotaBytesUsedAggregate,
							};
							var insert = await db.InsertAsync(ds);

							return true;
						}
					}
				}

				return false;

			} catch (Exception ex) {
				var e = ex.Message;
				return false;
			}
		}

		public static async Task<bool> RefreshToken(Datastore ds) {
			try {
				if (DateTime.Now < ds.TokenExpired) {
					return true;
				} else {
					var info = await GoogleDriveMyREST.RefreshToken (ds.RefreshToken);
					if (info != null) {
						ds.AccessToken = info.access_token;
						ds.TokenExpired = DateTime.Now.AddSeconds(info.expires_in);
						var db = DatabaseManager.GetNewAsyncConnection();
						var update = await db.UpdateAsync(ds);
						return update > 0;
					}
					return false;
				}

			} catch (Exception ex) {
				var m = ex.Message;
				return false;
			}
		}

		public async Task Refresh (Datastore ds, string path = "", bool recursive = false, bool createThumnails = false) {
			try {
				if (!await RefreshToken(ds)) {
					return;
				}

                var db = DatabaseManager.GetNewAsyncConnection();
                var archivos = await DatabaseManager.GetArchivos(ds.Id, db);

                try {
                    var info = await GoogleDriveMyREST.GetAccountInfo(ds.AccessToken);
                    if (info != null)
                    {
                        ds.FreeSpace = info.quotaBytesTotal - info.quotaBytesUsedAggregate;
                        await db.UpdateAsync(ds);
                    }
                }
                catch (Exception)
                { }

				var files = await GoogleDriveMyREST.GetFiles(ds);
				int cont = 0;
                int max = files.Count();


                foreach (var item in files)
				{
                    cont++;
					if (item.mimeType == "application/vnd.google-apps.folder") {
						continue;
					}
					//await Task.Delay(50);
                    var ar = archivos.Where(x => x.DatastoreFile_id == item.id).FirstOrDefault();
					if (ar == null) {
                        await Task.Delay(50);
                        ar = new Archivo {
							FileName = item.title,
							Size = (long)item.fileSize,
							Datastore_id = ds.Id,
							DatastorePath = item.downloadUrl,
							DatastoreFile_id = item.id,
							ThumbnailPath = item.thumbnailLink,
							MiniThumbnailPath = item.thumbnailLink,
                            MD5 = item.md5Checksum
						};
//						ar.ThumbnailPath = await ArchivoManager.GetThumbnailPath(ar);
//						ar.MiniThumbnailPath = await ArchivoManager.GetMiniThumbnailPath(ar);
//
//						if (createThumnails) {
//							var bytes = await DownloadThumbnail(dbx, item.PathLower);
//							if (bytes != null) {
//								ar.ThumbnailPath = ArchivoManager.SetThumbnail(ar, bytes);
//							}
//						}
						cont += await db.InsertAsync(ar);


						// autotag by extension
						ar = await DatabaseManager.GetArchivo(item.id, db);
						await SetTagByExtension(ar, db);
						// autotag by google mimetype
						string [] mimeDocument = {"application/vnd.google-apps.document", "application/vnd.google-apps.spreadsheet", "application/vnd.google-apps.presentation"};
						if (mimeDocument.Contains(item.mimeType)) {
							await DatabaseManager.AddTag("document", ar.Id, db);
						}

                        if (cont % 10 == 0)
                        {
                            Xamarin.Forms.MessagingCenter.Send<GoogleDriveManager, string>(this, "SyncMessage", cont + " / " + max);
                        }

                    }
				}
				
			} catch (Exception ex) {
				var m = ex.Message;
			}
		}

		public static async Task<byte[]> Download (Datastore ds, Archivo archivo)	{
			try {
				await RefreshToken(ds);

				return await GoogleDriveMyREST.GetFile(ds, archivo.DatastoreFile_id);

			} catch (Exception ex) {
				var e = ex.Message;
				return null;
			}
		}

		public static async Task<bool> DeleteFile (Datastore ds, Archivo archivo)	{
			try {
				await RefreshToken(ds);

				return await GoogleDriveMyREST.DeleteFile(ds, archivo.DatastoreFile_id);

			} catch (Exception ex) {
				var e = ex.Message;
				return false;
			}
		}

	}
}

