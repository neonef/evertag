﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Threading.Tasks;
using FFImageLoading.Forms;


namespace evertag
{
	public class ArchivoPage : ContentPage
	{
		Button addButton;
		Entry tagEntry;
		FolderPageItem fpi;
		Datastore ds;
		ListView tagListView;
		CachedImage  thumbnail;
		Archivo archivo;
		ObservableCollection<Tag> tagList = new ObservableCollection<Tag>();

		public ArchivoPage ()
		{
			Content = new StackLayout { 
				Children = {
					new Label { Text = "Hello ContentPage" }
				}
			};
		}
		public ArchivoPage (FolderPageItem myfpi, Datastore myds)
		{
			this.fpi = myfpi;
			this.ds = myds;
			Title = myfpi.Name;
		}

		public async Task InitUI () {


			addButton = new Button {
				Text = "+",
			};
			addButton.Clicked += OnAddButtonClicked;

			tagEntry = new Entry {
				Placeholder = "Add tag"	
			};
			tagEntry.Completed += OnTagEntryCompleted;

			await RefreshTagList();

			var cell = new DataTemplate(typeof(TextCell));
			cell.SetBinding (TextCell.TextProperty, "Name");


			tagListView = new ListView ();
			tagListView.ItemsSource = tagList;
			tagListView.ItemTemplate = cell;
		}

		public async Task RefreshTagList() {
			tagList.Clear ();
			foreach (var item in await ArchivoManager.GetArchivoTags (fpi.Id, DatabaseManager.GetNewAsyncConnection())) {
				tagList.Add (item);
			}
		}

		protected async override void OnAppearing ()
		{
			base.OnAppearing ();

			await InitUI ();

			try {
				archivo = await DatabaseManager.GetArchivo (fpi.Id, DatabaseManager.GetNewAsyncConnection());

				var h = App.ScreenHeight / 3;

				thumbnail = new CachedImage { 
//					Aspect = Aspect.AspectFit,
//					DownsampleToViewSize = true,
//					HeightRequest = h
				};
				thumbnail.Source = ImageSource.FromFile (await ArchivoManager.GetThumbnailPath (archivo, true));
				
				Title = fpi.Name;
				
				Content = new StackLayout { 
					Children = {
						thumbnail,
						new Label { Text = "Tags:" },
						tagEntry, 
						addButton,
						tagListView,
					}
				};
			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "Ok");
			}
		}

		async void OnAddButtonClicked (object sender, EventArgs e) {

			OnTagEntryCompleted (sender, e);
//			button.Text += "-";
//			var bytes = await DropboxManager.Download(
//				ds,
//				fpi.Path,
//				fpi.Name);
//			ArchivoManager.OpenFile(fpi.Name, bytes);
		}

		async void OnTagEntryCompleted(object sender, EventArgs e) {
			try {
				if (!string.IsNullOrWhiteSpace(tagEntry.Text.Trim())){
					if (await DatabaseManager.AddTag(tagEntry.Text.Trim(), archivo.Id, DatabaseManager.GetNewAsyncConnection())) {
						await RefreshTagList();
						tagEntry.Text = "";
					}

				}

			} catch (Exception ex) {
				await DisplayAlert ("Error", ex.Message, "OK");
			}
		}
	}
}


