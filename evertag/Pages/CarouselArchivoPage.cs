﻿using System;
using Xamarin.Forms;
using System.Collections.Generic;

namespace evertag
{
    public class CarouselArchivoPage : CarouselPage
	{
		public CarouselArchivoPage (List<FolderPageItem> itemsSource, int index)
		{
			this.ItemsSource = itemsSource;

			this.SelectedItem = itemsSource[index];


			this.ItemTemplate = new DataTemplate (() => {
				try {
					var nameLabel = new Label {
						FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
						HorizontalOptions = LayoutOptions.Center
					};
					nameLabel.SetBinding (Label.TextProperty, "Name");
					
					//				var colorBoxView = new BoxView {
					//					WidthRequest = 200,
					//					HeightRequest = 200,
					//					HorizontalOptions = LayoutOptions.Center,
					//					VerticalOptions = LayoutOptions.CenterAndExpand
					//				};
					//				colorBoxView.SetBinding (BoxView.ColorProperty, "Color");
					
					//				var thum = new CachedImage {
					//					Aspect = Aspect.AspectFill,
					//					DownsampleToViewSize = true,
					//					//HeightRequest = 1024
					//				};
					//				thum.SetBinding (CachedImage.SourceProperty, "Path");
					
					//				var thum = new Image {
					//					Aspect = Aspect.AspectFit,
					//					//DownsampleToViewSize = true,
					//					//HeightRequest = 1024
					//				};
					//				thum.SetBinding (Image.SourceProperty, "Path");
					
					return new ContentPage {
						Padding = new Thickness (0, Device.OnPlatform (40, 40, 0), 0, 0),
						Content = new StackLayout {
							Children = {
								nameLabel,
								//thum
							}
						}
					};
				} catch (Exception ex) {
					return ex;
				}
			});


		}
	}

	public class CarouselItem {
		public int Id {
			get;
			set;
		}

		public string Name {
			get;
			set;
		}

		public string Path {
			get;
			set;
		}

	}
}

