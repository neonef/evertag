﻿using Android.Content;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace evertag
{
	public class AddDatastorePage : ContentPage
	{
		TitleWebView browser;
		ListView listview;
        List<DatastorePageItem> list = new List<DatastorePageItem>();

        public AddDatastorePage ()
		{
			Title = "Register Datastore";

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

            list.Add(new DatastorePageItem
            {
                Type = DatastoreType.LocalStorage.ToString(),
                Source = "phone_128_b.png"
            });
            list.Add(new DatastorePageItem {
                Type = DatastoreType.Dropbox.ToString(),
                Source = "dropbox_200.png"
            });
            list.Add(new DatastorePageItem {
                Type = DatastoreType.GoogleDrive.ToString(),
                Source = "gdrive_512.png"
            });

            listview = new ListView {
				ItemsSource = list,
                RowHeight = 110,
                ItemTemplate = new DataTemplate(typeof(DatastoreCell))
            };
			listview.ItemSelected += async (object sender, SelectedItemChangedEventArgs e) => {
				var item = e.SelectedItem as DatastorePageItem;
				if (item != null) {
                    if (item.Type == DatastoreType.Dropbox.ToString())
                    {
                        Android.Widget.Toast.MakeText(Forms.Context, "Opening Dropbox login page ...", Android.Widget.ToastLength.Long).Show();
                        ShowDropboxLogin();
                    }
                    else if (item.Type == DatastoreType.GoogleDrive.ToString())
                    {
                        Android.Widget.Toast.MakeText(Forms.Context, "Opening Google Drive login page ...", Android.Widget.ToastLength.Long).Show();
                        ShowGoogleLogin();
                    }
                    else if (item.Type == DatastoreType.LocalStorage.ToString())
                    {
                        var nd = new Datastore
                        {
                            Name = "Local Storage",
                            Type = DatastoreType.LocalStorage,
                        };
                        var db = DatabaseManager.GetNewConnection();
                        var insert = db.Insert(nd);
                        if (insert > 0)
                        {
                            RefreshDatastores();
                            Android.Widget.Toast.MakeText(Forms.Context, "Local storage added succesfully", Android.Widget.ToastLength.Long).Show();
                            await Navigation.PopAsync();
                        }
                    }
                    else
                    {
						await DisplayAlert("", "Not implemented", "Ok");
					}

				}
			};

			Content = new StackLayout { 
				Children = {
					listview,
				}
			};

            BackgroundColor = Color.White;
        }

        public void RefreshDatastores()
        {
            Forms.Context.StartService(new Intent(Forms.Context, typeof(DropboxRefreshService)));
        }

		public void ShowDropboxLogin () {
			try {
				browser = new TitleWebView {
					Source = DropboxManager.LogoutUrl(),
					IsVisible = false,
				};

				browser.Navigated += async (object sender, WebNavigatedEventArgs e) => {
					var res = e.Result;

					if (e.Url == "https://www.dropbox.com/m/login?src=logout") {
						browser.Source = DropboxManager.LoginUrl();
						browser.IsVisible = true;
						return;
					}

					var response = DropboxManager.GetResponse(e.Url);
					if (response != null) {
						if (await DropboxManager.RegisterDatastoreInDB(response)) {
                            RefreshDatastores();
							await Navigation.PopAsync();
						}
					}

				};

				Content = browser;

			} catch (Exception ex) {
				DisplayAlert ("", ex.Message, "OK");
			}
		}

		public void ShowGoogleLogin () {
			try {
				browser = new TitleWebView {
					Source = GoogleDriveMyREST.GetLogoutUrl(),
					IsVisible = false,
				};

				browser.Navigated += (object sender, WebNavigatedEventArgs e) => {
					var res = e.Result;

					if (e.Url.StartsWith("https://accounts.google.com/ServiceLogin?elo=1")) {
						browser.Source = GoogleDriveMyREST.GetLoginUrl();
						browser.IsVisible = true;
						return;
					}

					if (e.Url.StartsWith("https://accounts.google.com/o/oauth2/approval")) {
						browser.Eval("window.location.assign(document.title)");
						return;
					}

				};

				browser.Navigating += async (object sender, WebNavigatingEventArgs e) => {
					if (e.Url.StartsWith("https://accounts.google.com/o/oauth2/Success")) {
						//https://accounts.google.com/o/oauth2/Success%20code=(the code)
						browser.IsVisible = false;
						string code = e.Url.Substring(e.Url.IndexOf("code=") + 5);
						string mensaje;
						if (await GoogleDriveManager.RegisterDatastoreInDB(code))
                        {
                            mensaje = "Google Drive account registered succesfully";
                            RefreshDatastores();
                        }
						else
							mensaje = "Error";

						Android.Widget.Toast.MakeText(Forms.Context, mensaje, Android.Widget.ToastLength.Long).Show();
						e.Cancel = true;
						await Navigation.PopAsync();

					} else if (e.Url.StartsWith("https://accounts.google.com/o/oauth2/Denied")) {
						//https://accounts.google.com/o/oauth2/Denied%20error=access_denied
						Android.Widget.Toast.MakeText(Forms.Context, "Permission denied", Android.Widget.ToastLength.Long).Show();
						e.Cancel = true;
						await Navigation.PopAsync();
					}

				};

				Content = browser;

			} catch (Exception ex) {
				var m = ex.Message;
			}

		}
	}
}


