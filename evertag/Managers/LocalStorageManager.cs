﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using PCLStorage;
using System.Linq;
using SQLite;
using FFImageLoading.Forms;
using Android.Graphics;
using System.IO;

namespace evertag
{
    public class LocalStorageManager : DatastoreManager
    {
        public static string[] AndroidLocalFolders = {
            Android.OS.Environment.DirectoryDcim,
            Android.OS.Environment.DirectoryDownloads,
            Android.OS.Environment.DirectoryMovies,
            Android.OS.Environment.DirectoryMusic,
            Android.OS.Environment.DirectoryPictures,
            Android.OS.Environment.DirectoryPodcasts,
            Android.OS.Environment.ExternalStorageDirectory.AbsolutePath,
        };

        public static string[] FoldersToAvoid = { "/data/data/", "/Android/data/" };
        public static string[] PhotoExtensions2 = { "jpg", "jpeg", "png" };

        public LocalStorageManager () {
            
        }

        public async Task Refresh(Datastore ds)
        {
            try
            {
                //var documentsDirectory = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
                //var appFolder = FileSystem.Current.LocalStorage;

                // sd0 -> sd interna
                var externalFolder = await FileSystem.Current.GetFolderFromPathAsync(
                    Android.OS.Environment.ExternalStorageDirectory.AbsolutePath
                );

                var sdCardFolder = await FileSystem.Current.GetFolderFromPathAsync("/storage/sdcard1");


                //var cameraFolder = await FileSystem.Current.GetFolderFromPathAsync(
                //    Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDcim).AbsolutePath
                //);

                //var rootFolder = await FileSystem.Current.GetFolderFromPathAsync(
                //    Android.OS.Environment.RootDirectory.AbsolutePath
                //);

                //var dos = await FileSystem.Current.GetFolderFromPathAsync("/mnt/sdcard2");
                //if (dos != null) {
                //var listados = await dos.GetFoldersAsync();
                //}

                var db = DatabaseManager.GetNewAsyncConnection();
                var archivos = await DatabaseManager.GetArchivos(ds.Id, db);


                //var externalFolderList = await externalFolder.GetFoldersAsync();
                //await SyncFolder(externalFolder, ds, archivos, db);
                var IFileLis = new List<IFile>();
                await GetAllFilesAsync(externalFolder, IFileLis);
                if (sdCardFolder != null)
                {
                    await GetAllFilesAsync(sdCardFolder, IFileLis);
                }

                //
                //var externalSDCardPath = GetExternalSDCardPath();


                //

                int cont = 0;
                int max = IFileLis.Count();
                bool next = false;

                foreach (var file in IFileLis)
                {
                    next = false;
                    foreach (string afolder in FoldersToAvoid)
                    {
                        if (file.Path.Contains(afolder))
                        {
                            next = true;
                        }
                    }
                    if (next) continue;


                    try
                    {
                        await Task.Delay(50);
                        
                            

                        var archivo = archivos.FirstOrDefault(x => x.LocalPath == file.Path);
                        if (archivo == null)
                        {
                            //var bytes = System.IO.File.ReadAllBytes(file.Path);

                            archivo = new Archivo
                            {
                                FileName = file.Name,
                                Size = new System.IO.FileInfo(file.Path).Length,
                                Datastore_id = ds.Id,
                                LocalPath = file.Path,
                                //MD5 = xBrainLab.Security.Cryptography.MD5.GetHashString(bytes),
                            };

                            int res = await db.InsertAsync(archivo);

                            // Thumbnails
                            var path = ArchivoManager.CalculateMiniThumbnailPath(archivo);
                            string extension = System.IO.Path.GetExtension(archivo.FileName).ToLower().Replace(".", "");
                            if (PhotoExtensions.Contains(extension))
                            {
                                var bytes = System.IO.File.ReadAllBytes(archivo.LocalPath);

                                int width = 256;

                                try
                                {
                                    Bitmap originalImage = await BitmapFactory.DecodeByteArrayAsync(bytes, 0, bytes.Length);
                                    bytes = new byte[0];

                                    float imageWidth = originalImage.Width;
                                    float imageHeight = originalImage.Height;
                                    float ratioHeight = (imageHeight * width) / imageWidth;
                                    
                                    Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)ratioHeight, false);
                                    originalImage.Dispose();
                                    
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        await resizedImage.CompressAsync(Bitmap.CompressFormat.Jpeg, 100, ms);
                                        System.IO.File.WriteAllBytes(path, ms.ToArray());
                                    }

                                    archivo.MiniThumbnailPath = path;
                                    await db.UpdateAsync(archivo);
                                }
                                catch (Exception ex)
                                {
                                    var m = ex.Message;
                                }

                            }
                            if (VideoExtensions.Contains(extension))
                            {
                                var bitmap = await Android.Media.ThumbnailUtils.CreateVideoThumbnailAsync(archivo.LocalPath, Android.Provider.ThumbnailKind.MiniKind);
                                using (MemoryStream ms = new MemoryStream())
                                {
                                    await bitmap.CompressAsync(Bitmap.CompressFormat.Jpeg, 100, ms);
                                    System.IO.File.WriteAllBytes(path, ms.ToArray());
                                }

                                archivo.MiniThumbnailPath = path;
                                await db.UpdateAsync(archivo);
                            }



                            // autotag by extension
                            await SetTagByExtension(archivo, db);
                            
                            // autotag by path
                            if (archivo.LocalPath.Contains("/WhatsApp"))
                                await DatabaseManager.AddTag("whatsapp", archivo.Id, db);

                        }

                        else
                        {

                        }
                    }
                    catch (Exception ex)
                    {
                        var m = ex.Message;
                    }


                    cont++;
                    if (cont % 10 == 0)
                    {
                        Xamarin.Forms.MessagingCenter.Send<LocalStorageManager, string>(this, "SyncMessage", cont + " / " + max);
                    }
                }
                
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }

        }

        private async Task GetAllFilesAsync(IFolder folder, List<IFile> IFileList)
        {
            try
            {
                var fileList = await folder.GetFilesAsync();
                foreach (var file in fileList)
                {
                    if (!file.Name.StartsWith(".") && AllExtensions.Contains(System.IO.Path.GetExtension(file.Name).ToLower().Replace(".", "")))
                    {
                        IFileList.Add(file);
                    }
                }

                var folderList = await folder.GetFoldersAsync();
                foreach (var subfolder in folderList)
                {
                    if (!subfolder.Name.StartsWith("."))
                    {
                        await GetAllFilesAsync(subfolder, IFileList);
                    }
                }

            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }
        }

        private async Task SyncFolder(IFolder folder, Datastore ds, List<Archivo> archivos, SQLiteAsyncConnection db) {
            try
            {
                var fileList = await folder.GetFilesAsync();
                foreach (var file in fileList)
                {
                    if (!file.Name.StartsWith(".") && AllExtensions.Contains(System.IO.Path.GetExtension(file.Name).ToLower().Replace(".", "")))
                    {
                        await SyncFile(file, ds, archivos, db);
                    }
                }

                var folderList = await folder.GetFoldersAsync();
                foreach (var subfolder in folderList)
                {
                    if (!subfolder.Name.StartsWith("."))
                    {
                        await SyncFolder(subfolder, ds, archivos, db);
                    }
                }

            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }
        }

        private async Task SyncFile(IFile file, Datastore ds, List<Archivo> archivos, SQLiteAsyncConnection db)
        {
            try
            {
                var archivo = archivos.FirstOrDefault(x => x.LocalPath == file.Path);
                if (archivo == null) {
                    //var b = file.
                    var bytes = System.IO.File.ReadAllBytes(file.Path);

                    archivo = new Archivo
                    {
                        FileName = file.Name,
                        Size = new System.IO.FileInfo(file.Path).Length,
                        Datastore_id = ds.Id,
                        LocalPath = file.Path,
                        MD5 = xBrainLab.Security.Cryptography.MD5.GetHashString(bytes),
                    };

                    int res = await db.InsertAsync(archivo);

                    string extension = System.IO.Path.GetExtension(archivo.FileName).ToLower().Replace(".", "");
                    if (PhotoExtensions.Contains(extension)) {
                        try
                        {
                            var path = ArchivoManager.CalculateMiniThumbnailPath(archivo);

                            var resize = ImageResizer.ResizeImage(bytes, 128, 128);
                            System.IO.File.WriteAllBytes(path, bytes);

                            archivo.MiniThumbnailPath = path;
                            await db.UpdateAsync(archivo);
                        }
                        catch (Exception ex)
                        {
                            var m = ex.Message;
                        }
                    }

                    // autotag by extension
                    //archivo = await DatabaseManager.GetArchivoByMD5(archivo.MD5, ds.Id, db);
                    await SetTagByExtension(archivo, db);

                    int cont = 0;
                    int max = 0;
                    if (cont % 10 == 0)
                    {
                        Xamarin.Forms.MessagingCenter.Send<LocalStorageManager, string>(this, "SyncMessage", cont + " / " + max);
                    }

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                var m = ex.Message;
            }
        }

        private string GetExternalSDCardPath() {
            string path = "";
            // for jellybean and below
            if (Android.OS.Build.VERSION.SdkInt <= Android.OS.BuildVersionCodes.JellyBeanMr2)
            {
                // and call exactly what we did before
                path = ExternalSdStorageHelper.GetExternalSdCardPath();
            }
            else // for kitkat and above
            {
                // I made a new method for legibility's sake 
                path = ExternalSdStorageHelper.GetExternalSdCardPathEx();
            }
            return path;
        }


    }
}
