﻿//http://www.appliedcodelog.com/2015/08/login-by-google-account-integration-for.html

using System;
using Xamarin.Auth;
using Xamarin.Forms;
using Android.Widget;

namespace evertag
{
    public class AndroidAuth
	{
		public AndroidAuth ()
		{
		}

		//LoginByGoogle in Xamrin.Android
		//Step 2.OAuth2Authenticator request
		public static void LoginByGoogle (bool allowCancel)
		{   
			var auth = new OAuth2Authenticator ( clientId: "30460193353-mlnd9637jh427nkeqt718hbhovk7bsl4.apps.googleusercontent.com" ,
				scope: "https://www.googleapis.com/auth/userinfo.email" ,
				authorizeUrl: new Uri ( "https://accounts.google.com/o/oauth2/auth" ),
				redirectUrl: new Uri ( "https://www.googleapis.com/plus/v1/people/me" ),
				//redirectUrl: new Uri( "https://www.googleapis.com/drive/v2/files"),
				getUsernameAsync: null );  	
			auth.AllowCancel = allowCancel;    

			auth.Completed += async (sender , e ) =>
			{  
				if ( !e.IsAuthenticated )
				{ 
					Toast.MakeText(Forms.Context,"Fail to authenticate!",ToastLength.Short).Show(); 
					return;
				}  
				string access_token;  
				e.Account.Properties.TryGetValue ( "access_token" , out access_token );  
				//step:3 Google API Request to get Profile Information
				if(await GoogleCommonClass.fnGetProfileInfoFromGoogle (access_token))
				{
					

					Toast.MakeText ( Forms.Context , "Authenticated successfully" , ToastLength.Short ).Show ();
				}
			};
			var intent = auth.GetUI ( Forms.Context );
			Forms.Context.StartActivity ( intent );
		}
	}
}

