﻿using SQLite;

namespace evertag
{
    public class Archivo_Tag
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		public int Archivo_id { get; set; }

		public int Tag_id { get; set; }
	}
}

