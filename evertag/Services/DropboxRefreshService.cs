﻿using System;
using Android.App;
using Android.Util;
using System.Threading;
using System.Collections.Generic;


//https://developer.xamarin.com/guides/android/application_fundamentals/services/part_1_-_started_services/
using Android.Content;

namespace evertag
{
	[Service]
	public class DropboxRefreshService : Service
	{
		public DropboxRefreshService ()
		{
			
		}

		public override StartCommandResult OnStartCommand (Android.Content.Intent intent, StartCommandFlags flags, int startId)
		{
			DoWork ();

			Log.Debug ("DropboxRefreshService", "DropboxRefreshService started");

			return StartCommandResult.RedeliverIntent;
		}

		public override Android.OS.IBinder OnBind (Android.Content.Intent intent)
		{
			return null;
		}

		public void DoWork ()
		{
			var t = new Thread (async () => {
                //				Log.Debug ("DemoService", "Doing work");
                //				Thread.Sleep (5000);
                //				Log.Debug ("DemoService", "Work complete");

                string text = "";

				try {
                    Xamarin.Forms.MessagingCenter.Subscribe <DropboxManager, string>(this, "SyncMessage", (sender, arg) => {
                        SendNotification("Evertag", text + " " + arg);
                    });
                    Xamarin.Forms.MessagingCenter.Subscribe<GoogleDriveManager, string>(this, "SyncMessage", (sender, arg) => {
                        SendNotification("Evertag", text + " " + arg);
                    });
                    Xamarin.Forms.MessagingCenter.Subscribe<LocalStorageManager, string>(this, "SyncMessage", (sender, arg) => {
                        SendNotification("Evertag", text + " " + arg);
                    });


                    List<Datastore> dslist;
					var db = DatabaseManager.GetNewAsyncConnection();
					dslist = await db.Table<Datastore> ().ToListAsync();
                    foreach (var ds in dslist)
                    {
                        text = ds.Name + " is syncing";
                        if (ds.Type == DatastoreType.GoogleDrive)
                        {
                            SendNotification("Evertag", text);
                            var gdm = new GoogleDriveManager();

                            await gdm.Refresh(ds);

                            text = ds.Name + " sync finished";
                            SendNotification("Evertag", text);

                        }
                        else if (ds.Type == DatastoreType.Dropbox)
                        {
                            SendNotification("Evertag", text);
                            var dbm = new DropboxManager();

                            await dbm.Refresh2(ds, "", true, true);

                            text = ds.Name + " sync finished";
                            SendNotification("Evertag", text);

                        }
                        else if (ds.Type == DatastoreType.LocalStorage)
                        {
                            SendNotification("Evertag", text);

                            FFImageLoading.Forms.Droid.CachedImageRenderer.Init();

                            var lm = new LocalStorageManager();

                            await lm.Refresh(ds);

                            text = ds.Name + " sync finished";
                            SendNotification("Evertag", text);

                        }
                    }
                    var m = "fin";

				} catch (Exception ex) {
					var m = ex.Message;
				}
					StopSelf ();
			});

			t.Start ();
		}

        void SendNotification(string title, string text)
        {
            var nMgr = (NotificationManager)GetSystemService(NotificationService);
            //var notification = new Notification (Resources.GetDrawable. "Message from demo service");
            var notification = new Notification(evertag.Droid.Resource.Drawable.tags_24_grey, title);
            var pendingIntent = PendingIntent.GetActivity(this, 0, new Intent(this, typeof(evertag.Droid.MainActivity)), 0);
            notification.SetLatestEventInfo(this, title, text, pendingIntent);
            nMgr.Notify(0, notification);
        }


    }
}

