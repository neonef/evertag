﻿using Xamarin.Forms.Platform.Android;
using Android.Webkit;
using Xamarin.Forms;

namespace evertag
{
    public class TitleWebViewRenderer : WebViewRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement == null)
			{
				Control.SetWebViewClient(new TitleWebViewClient(this));
			}
		}

		internal class TitleWebViewClient : WebViewClient
		{
			readonly TitleWebViewRenderer titleWebViewRenderer;

			internal TitleWebViewClient(TitleWebViewRenderer titleWebViewRenderer)
			{
				this.titleWebViewRenderer = titleWebViewRenderer;
			}

			public override void OnPageFinished(Android.Webkit.WebView view, string url)
			{
				base.OnPageFinished(view, url);
				((IElementController) titleWebViewRenderer.Element).SetValueFromRenderer(TitleWebView.PageTitleProperty, view.Title);
			}
		}
	}
}


//// IOS renderer:
//public class TitleWebViewRenderer : WebViewRenderer
//{
//	public TitleWebViewRenderer()
//	{
//		this.LoadFinished += (object sender, EventArgs e) => {
//			var titleWebView = (TitleWebView)Element;
//			((IElementController)Element).SetValueFromRenderer(TitleWebView.PageTitleProperty, 
//				EvaluateJavascript("document.title"));
//		};
//	}
//}