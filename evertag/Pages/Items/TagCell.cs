﻿using Xamarin.Forms;
using FFImageLoading.Forms;
using Android.Widget;

namespace evertag
{
    public class TagCell : ViewCell
	{
		public CachedImage cachedImage = null;
		public int archivo_id;

		public TagCell ()
		{
			

			cachedImage = new CachedImage() {
				HorizontalOptions = LayoutOptions.Center,
				VerticalOptions = LayoutOptions.Center,
				//WidthRequest = 50,
				HeightRequest = 40,
				//CacheDuration = TimeSpan.FromDays(30),
				DownsampleToViewSize = true,
				RetryCount = 0,
				RetryDelay = 250,
				TransparencyEnabled = false,
				LoadingPlaceholder = "loading.png",
				ErrorPlaceholder = "error.png",
				//Source = "tag_128.png",
			};

			cachedImage.SetBinding (CachedImage.SourceProperty, "Source");

			StackLayout cellWrapper = new StackLayout ();
			StackLayout horizontalLayout = new StackLayout ();
			Label left = new Label {
                TextColor = Color.Black
            };

			left.SetBinding (Label.TextProperty, "Name");
			left.VerticalOptions = LayoutOptions.Center;

			horizontalLayout.Orientation = StackOrientation.Horizontal;

			horizontalLayout.Children.Add (cachedImage);
			horizontalLayout.Children.Add (left);

			cellWrapper.Children.Add (horizontalLayout);
			View = cellWrapper;

			// Menu
			var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true }; // red background
			deleteAction.SetBinding (MenuItem.CommandParameterProperty, "archivoid_tagid");
			deleteAction.Clicked += async (sender, e) => {
				var mi = ((MenuItem)sender);
				var archivo_id = ((int[])mi.CommandParameter)[0];
				var tag_id = ((int[])mi.CommandParameter)[1];
				var db = DatabaseManager.GetNewAsyncConnection();
				var tag = await DatabaseManager.GetTag(tag_id, db);
				var res = await DatabaseManager.DeleteTag(tag_id, db);
				Toast.MakeText(Forms.Context,"Tag '" + tag.Name + "' deleted",ToastLength.Short).Show(); 

				MessagingCenter.Send<TagCell, int> (this, "DeleteTag", tag_id);
			};
			ContextActions.Add (deleteAction);

		}
	}

}

