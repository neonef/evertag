using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace evertag
{
	public class FolderPage : ContentPage
	{
		//List<FolderPageItem> list;
		// UI
		ListView listview;
		Label label;
		Entry entry = new Entry { Placeholder = "Write a tag"};
		SearchBar searchBar;
		bool firstShow = true;
        int exitCount = 0;

		int winit = 0;
        int delta = 100;
		int wend = 0;



		// Navigation
		FolderPageBrowseMode Mode = FolderPageBrowseMode.ByFolder;
		Datastore DS;
		string path = string.Empty;
		public static List<Tag> TagList;
		//List<FolderPageItem> itemsSource;
		ObservableCollection<FolderPageItem> itemsSource = new ObservableCollection<FolderPageItem> ();

		Carousel4 myCarousel = null;
		public bool closeCarousel = false;

        FolderPageItem lastItemShowed;
        private bool _canClose = true;

        public FolderPage (FolderPageBrowseMode Mode, string path = "", Datastore DS = null, List<Tag> ATagList = null) {

			this.Mode = Mode;
			this.path = path;
			this.DS = DS;
			TagList = ATagList;

			Title = "Browse by Tag";

            NavigationPage.SetTitleIcon(this, "tags_128_bw.png");

			entry.TextChanged += async (object sender, TextChangedEventArgs e) => {
				await UpdateContent();
			};

			searchBar = new SearchBar {
				Placeholder = "Write to filter",
			};
			searchBar.TextChanged += async (object sender, TextChangedEventArgs e) => {
				if (e.NewTextValue == string.Empty)
					await UpdateContent();
			};
			searchBar.SearchButtonPressed += async (object sender, EventArgs e) => {
				searchBar.Text = searchBar.Text.ToLower();
				await UpdateContent();
			};


			listview = new ListView (ListViewCachingStrategy.RecycleElement) {
				ItemsSource = itemsSource,
				IsPullToRefreshEnabled = true,
				ItemTemplate = new DataTemplate(typeof(MyImageCell)),
				BackgroundColor = Color.White,
				RowHeight = 110,
				//HeightRequest = 200,
				SeparatorColor = Color.Blue,
			};

            listview.ItemSelected += OnItemSelected;
			listview.Refreshing += async (sender, e) => {
				//DatastoreManager.RefreshAll();
				await UpdateContent();
				((ListView)sender).IsRefreshing = false;
			};
            listview.ItemAppearing += Listview_ItemAppearing;

			label = new Label { Text = "" };

			Content = new StackLayout {
				Children = {
					//entry,
					searchBar,
					listview,
					label,
				}
			};

			BackgroundColor = Color.White;


		}

        private void Listview_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var cell = e.Item as FolderPageItem;
            if (cell.Type == FolderPageItemType.Archivo) {
                int total = itemsSource.Count;
                if (total >= 200) {
                    int lastItemPos = itemsSource.IndexOf(lastItemShowed);
                    int currentItemPos = itemsSource.IndexOf(cell);
                }
                lastItemShowed = cell;
            }
        }

        protected async override void OnAppearing ()
		{
			base.OnAppearing ();
			if (firstShow) {
				firstShow = false;
				await UpdateContent ();
			}

		}

		async void OnItemSelected (object sender, SelectedItemChangedEventArgs e)
		{
			try {
				var item = e.SelectedItem as FolderPageItem;
				if (item != null) {
                    if (item.Type == FolderPageItemType.Datastore)
                    {
                        var ds = await DatabaseManager.GetDatastore(item.Id, DatabaseManager.GetNewAsyncConnection());
                        //await SetContent(FolderPageBrowseMode.ByFolder, item.Path, ds);
                        await Navigation.PushAsync(new FolderPage(FolderPageBrowseMode.ByFolder, item.Path, ds));
                    }
                    else if (item.Type == FolderPageItemType.Folder)
                    {
                        var ds = await DatabaseManager.GetDatastore(item.Datastore_id, DatabaseManager.GetNewAsyncConnection());
                        //await SetContent(FolderPageBrowseMode.ByFolder, item.Path, ds);
                        await Navigation.PushAsync(new FolderPage(FolderPageBrowseMode.ByFolder, item.Path, ds));
                    }
                    else if (item.Type == FolderPageItemType.Archivo)
                    {

                        var archivoList = itemsSource.Where(x => x.Type == FolderPageItemType.Archivo).ToList();

                        if (myCarousel != null)
                        {
                            myCarousel.Children.Clear();
                            myCarousel = null;
                        }

                        myCarousel = new Carousel4(archivoList, item);
                        myCarousel.folderPage = this;
                        await Navigation.PushAsync(myCarousel);


                    }
                    else if (item.Type == FolderPageItemType.Tag)
                    {
                        if (TagList == null)
                            TagList = new List<Tag>();
                        TagList.Add(item.tag);
                        //await Navigation.PushAsync (new FolderPage (FolderPageBrowseMode.ByTag, null, null, TagList));
                        await UpdateContent();
                    }
                    else if (item.Type == FolderPageItemType.Tag)
                    {
                        await Navigation.PushAsync(new FolderPage(FolderPageBrowseMode.ByTag, null, null, null));
                    }
                    else if (item.Type == FolderPageItemType.Next)
                    {
                        itemsSource.RemoveAt(itemsSource.Count - 1);
                        var lastItem = itemsSource.Last();

                        var db = DatabaseManager.GetNewAsyncConnection();
                        var list = await DatabaseManager.GetArchivos(TagList, db, wend, delta);
                        foreach (var ar in list)
                        {
                            itemsSource.Add(new FolderPageItem
                            {
                                Id = ar.Id,
                                Name = ar.FileName,
                                Type = FolderPageItemType.Archivo,
                                archivo = ar,
                                Path = await ArchivoManager.GetMiniThumbnailPath(ar),
                                //Path = ar.MiniThumbnailPath,
                                Size = (ulong)ar.Size
                            });
                            wend++;
                        }

                        if (itemsSource.Count >= delta * 3)
                        {
                            int i = 0;
                            while (itemsSource[i].Type != FolderPageItemType.Prev && itemsSource[i].Type != FolderPageItemType.Archivo) i++;
                            winit += delta;
                            for (int j = delta + i - 1; j >= i; j--)
                            {
                                itemsSource.RemoveAt(j);
                            }

                            itemsSource.Insert(i, new FolderPageItem
                            {
                                Type = FolderPageItemType.Prev,
                                Name = "Previous files",
                            });
                        }

                        var nextList = await DatabaseManager.GetArchivos(TagList, db, wend, 1);
                        if (nextList.ToList().Count > 0)
                        {
                            itemsSource.Add(new FolderPageItem
                            {
                                Type = FolderPageItemType.Next,
                                Name = "Next files",
                            });
                        }
                        

                        listview.ScrollTo(lastItem, ScrollToPosition.MakeVisible, false);

                        label.Text = itemsSource.Count() + " items found";
                    }
                    else if (item.Type == FolderPageItemType.Prev)
                    {
                        var prevIem = itemsSource.FirstOrDefault(x => x.Type == FolderPageItemType.Prev);
                        if (prevIem != null)
                            itemsSource.Remove(prevIem);

                        var firstArchivoItem = itemsSource.FirstOrDefault(x => x.Type == FolderPageItemType.Archivo);
                        var i = itemsSource.IndexOf(firstArchivoItem);
                        
                        var db = DatabaseManager.GetNewAsyncConnection();
                        var list = await DatabaseManager.GetArchivos(TagList, db, winit - delta, delta);
                        foreach (var ar in list.Reverse())
                        {
                            itemsSource.Insert(i, new FolderPageItem
                            {
                                Id = ar.Id,
                                Name = ar.FileName,
                                Type = FolderPageItemType.Archivo,
                                archivo = ar,
                                Path = await ArchivoManager.GetMiniThumbnailPath(ar),
                                //Path = ar.MiniThumbnailPath,
                                Size = (ulong)ar.Size
                            });
                            winit--;
                        }

                        var numArchivosItemSource = itemsSource.Count(x => x.Type == FolderPageItemType.Archivo);
                        while (numArchivosItemSource > delta * 3) {
                            itemsSource.RemoveAt(itemsSource.Count - 1);
                            numArchivosItemSource--;
                            wend--;
                        }

                        if (winit > 0)
                        {
                            var prevlist = await DatabaseManager.GetArchivos(TagList, db, winit - 1, 1);
                            if (prevlist != null && prevlist.ToList().Count > 0)
                            {
                                itemsSource.Insert(i, new FolderPageItem
                                {
                                    Type = FolderPageItemType.Prev,
                                    Name = "Previous files",
                                });
                            }
                        }

                        var nextList = await DatabaseManager.GetArchivos(TagList, db, wend, 1);
                        if (nextList.ToList().Count > 0)
                        {
                            itemsSource.Add(new FolderPageItem
                            {
                                Type = FolderPageItemType.Next,
                                Name = "Next files",
                            });
                        }


                        listview.ScrollTo(firstArchivoItem, ScrollToPosition.MakeVisible, false);

                        label.Text = itemsSource.Count() + " items found";
                    }

                    listview.SelectedItem = null;
				}
			} catch (Exception ex) {
				var m = ex.Message;
			}
		}

		public async Task UpdateContent() {
            //listview = null;
            exitCount = 0;
            winit = 0;
			wend = delta;



			try {
				itemsSource.Clear ();
			
				if (Mode == FolderPageBrowseMode.ByTag) {
				
					var db = DatabaseManager.GetNewAsyncConnection ();
				
					itemsSource.Clear ();
				
					if (TagList != null && TagList.Count > 0) {
						
						itemsSource.Add (new FolderPageItem {
							Name = "[" + TagManager.TagListAsStringNames (TagList) + "]", 
							Type = FolderPageItemType.Text,
							Path = "tag_checked_128.png",
						});
					} else {
						//					// Without tags
						//					itemsSource.Add (new FolderPageItem {
						//						Name = "[Without tags]", 
						//						Type = FolderPageItemType.Untagged,
						//						Path = "tag1.jpg",
						//					});
					
					}
				
				
					var taglist = await DatabaseManager.GetArchivoTags (db, TagList);
					// Filter tag list
					//string pattern = entry.Text ?? "";
					string pattern = searchBar.Text ?? "";
					pattern = pattern.Trim ();
					if (!string.IsNullOrEmpty (pattern)) {
						taglist = taglist.Where (t => t.Name.Contains (pattern)).ToList ();
					}
				
				
					foreach (var item in taglist) {
						itemsSource.Add (new FolderPageItem {
							Id = item.Id,
							Name = item.Name, 
							Type = FolderPageItemType.Tag,
							archivo = null,
							tag = item,
							Path = "tag_empty_128.png",
						});
					}
				
					var list = await DatabaseManager.GetArchivos (TagList, db, winit, wend - 1);
					foreach (var item in list) {
						itemsSource.Add (new FolderPageItem {
							Id = item.Id,
							Name = item.FileName, 
							Type = FolderPageItemType.Archivo,
							archivo = item,
							Path = await ArchivoManager.GetMiniThumbnailPath(item),
							//Path = item.MiniThumbnailPath,
							Size = (ulong)item.Size
						});
					}

                    var nextList = await DatabaseManager.GetArchivos(TagList, db, wend, 1);
                    if (nextList.ToList().Count > 0)
                    {
                        itemsSource.Add(new FolderPageItem
                        {
                            Type = FolderPageItemType.Next,
                            Name = "Next files",
                        });
                    }

                    label.Text = list.Count () + " items found";

				}


			} catch (Exception ex) {
				var m = ex.Message;
			}

		}

        protected override bool OnBackButtonPressed()
        {
            if (Mode == FolderPageBrowseMode.ByTag)
            {
                if (TagList == null || TagList.Count == 0)
                {
                    if (exitCount == 1)
                    {
                        exitCount = 0;
                        return false;
                    }
                    else
                    {
                        Android.Widget.Toast.MakeText(Forms.Context, "Press Back again to quit", Android.Widget.ToastLength.Short).Show();
                        exitCount++;
                    }
                }
                else
                {
                    TagList.RemoveAt(TagList.Count - 1);
                    if (TagList.Count == 0)
                    {
                        TagList = null;
                    }
                    UpdateContent();
                }
            }
            return true;
        }
        

	}

	public enum FolderPageBrowseMode {
		ByFolder, ByTag
	}

}

