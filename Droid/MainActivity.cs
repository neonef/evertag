﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using FFImageLoading.Forms.Droid;

namespace evertag.Droid
{
    [Activity (Label = "evertag", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			global::Xamarin.Forms.Forms.Init (this, bundle);

			CachedImageRenderer.Init();

			var ScreenWidth = (int)Resources.DisplayMetrics.WidthPixels; // real pixels
			var ScreenHeight = (int)Resources.DisplayMetrics.HeightPixels; // real pixels

			LoadApplication (new App (ScreenWidth, ScreenHeight));
		}
	}
}

