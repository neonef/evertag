﻿using Xamarin.Forms;
//using FFImageLoading.Forms.Droid;

namespace evertag
{
    public class App : Application
	{
		public static int ScreenWidth;
		public static int ScreenHeight;

		public App (int myScreenWidth, int myScreenHeight)
		{
            // The root page of your application
            //MainPage = new MasterPage();

#if __ANDROID__
            FFImageLoading.Forms.Droid.CachedImageRenderer.Init();
#elif __IOS__
            FFImageLoading.Forms.Touch.CachedImageRenderer.Init();
#endif

            //CachedImageRenderer.Init();

            ScreenWidth = myScreenWidth;
			ScreenHeight = myScreenHeight;

			DatabaseManager.CheckDB ();

			MainPage = new MainPage() { BackgroundColor = Color.White };

		}

		protected override void OnStart ()
		{
			// Handle when your app starts
			//Prueba();
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
			//Prueba();
		}

	}


}

